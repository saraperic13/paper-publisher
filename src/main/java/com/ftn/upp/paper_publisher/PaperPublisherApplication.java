package com.ftn.upp.paper_publisher;

import com.ftn.upp.paper_publisher.service.init.InitService;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@EnableProcessApplication
@EnableJpaAuditing
public class PaperPublisherApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(PaperPublisherApplication.class, args);
        context.getBean(InitService.class).init();
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}

