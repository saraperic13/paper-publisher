package com.ftn.upp.paper_publisher.utils;

public class HtmlUtils {

    public static String process(String html){
        html = html.replaceAll("\\$", "");

        html = html.replaceAll("ng-if", "*ngIf");
        html = html.replaceAll("ng-show", "*ngShow");
        return html;
    }
}
