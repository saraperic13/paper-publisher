package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.ScientificPaperVersion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScientificPaperVersionDao extends JpaRepository<ScientificPaperVersion, Long> {
}
