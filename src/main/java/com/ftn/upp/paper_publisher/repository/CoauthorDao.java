package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Coauthor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoauthorDao extends JpaRepository<Coauthor, Long> {
}
