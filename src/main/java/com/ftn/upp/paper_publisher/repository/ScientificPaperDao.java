package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.ScientificPaper;
import com.ftn.upp.paper_publisher.domain.ScientificPaperState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ScientificPaperDao extends JpaRepository<ScientificPaper, Long> {

    Optional<ScientificPaper> findById(Long id);

    List<ScientificPaper> findAllByJournalIdAndState(Long journalId, ScientificPaperState state);

    Optional<ScientificPaper> findByProcessInstanceId(String processInstanceId);
}
