package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Editor;
import com.ftn.upp.paper_publisher.domain.EditorialBoard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EditorialBoardDao extends JpaRepository<EditorialBoard, Long> {

    Optional<EditorialBoard> getByChiefEditorId(Long id);
}
