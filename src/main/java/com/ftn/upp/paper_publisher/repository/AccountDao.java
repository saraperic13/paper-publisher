package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.auth.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountDao extends JpaRepository<Account, Long> {

    Optional<Account> findByUsername(String email);

}
