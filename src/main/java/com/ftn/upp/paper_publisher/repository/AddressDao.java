package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressDao extends JpaRepository<Address, Long> {

}
