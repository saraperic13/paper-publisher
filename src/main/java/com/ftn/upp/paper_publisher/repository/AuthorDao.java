package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorDao extends JpaRepository<Author, Long> {

    Optional<Author> getByUsername(String username);
}
