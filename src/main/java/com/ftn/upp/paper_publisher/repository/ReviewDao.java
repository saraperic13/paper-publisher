package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Review;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewDao extends JpaRepository<Review, Long> {

    List<Review> getAllByScientificPaperId(Long paperId);

    List<Review> getAllByProcessInstanceId(String processInstanceId);
}
