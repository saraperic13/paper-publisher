package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Editor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EditorDao extends JpaRepository<Editor, Long> {

    Optional<Editor> getByUsername(String username);
}
