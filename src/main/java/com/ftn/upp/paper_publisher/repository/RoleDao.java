package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.auth.Role;
import com.ftn.upp.paper_publisher.domain.auth.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Long> {

    Role getByRoleType(RoleType roleType);
}
