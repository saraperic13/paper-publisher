package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Membership;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MembershipDao extends JpaRepository<Membership, Long> {

    Membership getByJournalIssnAndAccountUsername(String issn, String username);
}
