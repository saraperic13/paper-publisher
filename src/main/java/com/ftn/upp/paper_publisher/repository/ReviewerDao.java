package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Reviewer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ReviewerDao extends JpaRepository<Reviewer, Long> {

    Optional<Reviewer> getById(Long id);

    Optional<Reviewer> getByUsername(String username);
}
