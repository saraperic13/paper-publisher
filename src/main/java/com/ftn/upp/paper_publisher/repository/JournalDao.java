package com.ftn.upp.paper_publisher.repository;

import com.ftn.upp.paper_publisher.domain.Journal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JournalDao extends JpaRepository<Journal, Long> {

    Optional<Journal> getById(Long id);

    Optional<Journal> getByIssn(String issn);
}
