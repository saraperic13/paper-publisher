package com.ftn.upp.paper_publisher.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class NoAuthentication extends RuntimeException {
    private String message;
}
