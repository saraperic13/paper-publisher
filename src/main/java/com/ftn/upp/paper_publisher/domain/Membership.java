package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.audit.AuditModel;
import com.ftn.upp.paper_publisher.domain.auth.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Membership extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Journal journal;

    @OneToOne
    private Account account;

    private LocalDateTime activeUntil;

}
