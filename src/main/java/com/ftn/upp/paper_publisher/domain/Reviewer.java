package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.auth.Account;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class Reviewer extends Account {

    private String title;

    @JoinTable(name = "ReviewerScienceFields", joinColumns = @JoinColumn(name = "account_id"))
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = ScientificField.class)
    @Column(name = "science_fields", nullable = false)
    private List<ScientificField> scientificFields = new ArrayList<>();
}
