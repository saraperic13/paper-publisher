package com.ftn.upp.paper_publisher.domain;

public enum ReviewAdviceType {
    ACCEPT, REJECT, ACCEPT_SMALL_CHANGE, ACCEPT_BIG_CHANGE
}
