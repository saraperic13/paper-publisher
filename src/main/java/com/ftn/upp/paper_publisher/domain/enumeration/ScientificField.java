package com.ftn.upp.paper_publisher.domain.enumeration;

public enum ScientificField {
    GENERAL_SCIENCE, PHYSICAL_SCIENCE, LIFE_SCIENCE, ENGINEERING, MATHEMATICS, MEDICINE, COMPUTER_SCIENCE
}
