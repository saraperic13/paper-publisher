package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.audit.AuditModel;
import com.ftn.upp.paper_publisher.domain.enumeration.JournalType;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Journal extends AuditModel {

    @Id
    @Column(name = "journal_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private String issn;

    @JoinTable(name = "JournalScienceFields", joinColumns = @JoinColumn(name = "journal_id"))
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = ScientificField.class)
    @Column(name = "science_fields", nullable = false)
    private List<ScientificField> scientificField;

    @OneToOne
    @JoinColumn(name = "editorial_board_id")
    private EditorialBoard editorialBoard;

    @OneToMany
    @JoinTable(name = "JournalReviewers",
            joinColumns = @JoinColumn(name = "journal_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id"))
    private List<Reviewer> reviewers = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "journal_type", nullable = false)
    private JournalType journalType;

    private String paymentId;
}
