package com.ftn.upp.paper_publisher.domain.enumeration;

public enum JournalType {
    OPEN_ACCESS, SUBSCRIPTION
}
