package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.auth.Account;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Author extends Account {

    @ManyToMany
    @JoinTable(name = "author_scientific_papers",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "scientific_paper_id"))
    private List<ScientificPaper> scientificPapers = new ArrayList<>();

    public Author(Account account) {
        this.setUsername(account.getUsername());
        this.setLastName(account.getLastName());
        this.setFirstName(account.getFirstName());
        if (account.getAddress() != null) {
            this.setAddress(account.getAddress());
        }
        this.setRoles(account.getRoles());
        this.setPassword(account.getPassword());
    }

}
