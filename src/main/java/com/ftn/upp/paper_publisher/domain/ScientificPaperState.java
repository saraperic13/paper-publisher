package com.ftn.upp.paper_publisher.domain;

public enum ScientificPaperState {
    SUBMITTED, ON_REVIEW, ON_REVISION, REJECTED, ACCEPTED
}
