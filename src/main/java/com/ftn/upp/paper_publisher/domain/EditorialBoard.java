package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EditorialBoard extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "editorial_board_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "chief_editor_id", nullable = false)
    private Editor chiefEditor;

    @OneToMany
    @JoinTable(name = "EditorialBoardEditors",
            joinColumns = @JoinColumn(name = "editorial_board_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id"))
    private List<Editor> editors = new ArrayList<>();

    @OneToOne
    @JoinColumn(name="journal_id", nullable = false)
    private Journal journal;


}
