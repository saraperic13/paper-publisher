package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.audit.AuditModel;
import com.ftn.upp.paper_publisher.domain.auth.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Review extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Account reviewer;

    @Column(nullable = false)
    private String comments;

    @ManyToOne
    private ScientificPaper scientificPaper;

    @Enumerated(EnumType.STRING)
    private ReviewAdviceType reviewAdviceType;

    private String commentsForEditors;

}
