package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScientificPaperVersion extends AuditModel {

    @Id
    @Column(name = "version_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String filePath;

    @Column(name = "final", columnDefinition = "BOOL DEFAULT FALSE")
    private boolean finalVersion;

}
