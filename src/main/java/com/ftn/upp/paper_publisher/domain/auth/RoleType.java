package com.ftn.upp.paper_publisher.domain.auth;

public enum RoleType {
    EDITOR, CHIEF_EDITOR, REVIEWER, AUTHOR
}
