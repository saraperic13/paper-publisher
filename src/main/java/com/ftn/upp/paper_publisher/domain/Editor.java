package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.auth.Account;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Editor extends Account {

    private String title;

    @JoinTable(name = "EditorScienceFields", joinColumns = @JoinColumn(name = "account_id"))
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = ScientificField.class)
    @Column(name = "science_fields")
    private List<ScientificField> scientificField = new ArrayList<>();

    @ManyToOne
    private EditorialBoard editorialBoard;

}
