package com.ftn.upp.paper_publisher.domain;

import com.ftn.upp.paper_publisher.domain.audit.AuditModel;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ScientificPaper extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "scientific_paper_id")
    private Long id;

    @Column(nullable = false)
    private String name;

    @ElementCollection(targetClass = String.class)
    private List<String> keywords = new ArrayList<>();

    @Column(name = "abstract")
    private String paperAbstract;

    @ManyToOne
    private Author author;

    @OneToMany
    @JoinTable(name = "ScientificPaperCoauthor",
            joinColumns = @JoinColumn(name = "scientific_paper_id"),
            inverseJoinColumns = @JoinColumn(name = "coauthor_id"))
    private List<Coauthor> coauthors = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "ScientificPaperVersions",
            joinColumns = @JoinColumn(name = "scientific_paper_id"),
            inverseJoinColumns = @JoinColumn(name = "version_id"))
    private List<ScientificPaperVersion> versions = new ArrayList<>();

    @ManyToOne
    private Journal journal;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ScientificField scientificField;

    @Enumerated(EnumType.STRING)
    private ScientificPaperState state;

    private Double price;

}
