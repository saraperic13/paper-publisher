package com.ftn.upp.paper_publisher.security;

import com.ftn.upp.paper_publisher.exception.NoAuthentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class UserService {

    public String getCurrentUsername() {

        try {
            final User userDetails = (User) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            return userDetails.getUsername();
        } catch (Exception e) {
            throw new NoAuthentication();
        }
    }
}
