package com.ftn.upp.paper_publisher.security;

import com.ftn.upp.paper_publisher.domain.auth.Account;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.AccountDao;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class TokenUtils {

    @Value("${utils.token.secret}")
    private String secret;

    @Value("${utils.token.expiration}")
    private Long expiration;

    private final AccountDao accountDao;

    @Autowired
    public TokenUtils(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Transactional
    public String generateToken(UserDetails userDetails) {
        final Map<String, Object> claims = new HashMap<>();
        claims.put("sub", userDetails.getUsername());
        claims.put("created", new Date(System.currentTimeMillis()));

        final Account account = this.accountDao.findByUsername(userDetails.getUsername()).orElseThrow(
                () -> new EntityDoesNotExistException("User with specified username does not exist!"));

        claims.put("userId", account.getId());
        claims.put("roles", account.getRoles().stream().map(role -> role.getRoleType().name()).collect(Collectors.toList()));
        return Jwts.builder().setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    String getUsernameFromToken(String token) {
        String username;
        try {
            Claims claims = this.getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return username.equals(userDetails.getUsername())
                && !isTokenExpired(token);
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(this.secret)
                    .parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    private Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = this.getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    private boolean isTokenExpired(String token) {
        final Date expiration = this.getExpirationDateFromToken(token);
        return expiration.before(new Date(System.currentTimeMillis()));
    }
}

