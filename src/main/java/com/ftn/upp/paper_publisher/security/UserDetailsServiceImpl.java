package com.ftn.upp.paper_publisher.security;

import com.ftn.upp.paper_publisher.domain.auth.Account;
import com.ftn.upp.paper_publisher.domain.auth.Role;
import com.ftn.upp.paper_publisher.repository.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AccountDao accountDao;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final Account account = accountDao.findByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format("No user found with username '%s'.", username)));

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (Role role : account.getRoles()) {
            grantedAuthorities.addAll(role.getPermissions().stream()
                    .map(permission -> new SimpleGrantedAuthority(permission.getName()))
                    .collect(Collectors.toList()));
        }

        return new org.springframework.security.core.userdetails.User(
                account.getUsername(),
                account.getPassword(),
                grantedAuthorities);

    }

}

