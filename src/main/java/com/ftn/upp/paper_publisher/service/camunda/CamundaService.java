package com.ftn.upp.paper_publisher.service.camunda;

import com.ftn.upp.paper_publisher.exception.CamundaException;
import com.google.gson.Gson;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CamundaService {

    private final RestTemplate restTemplate;
    private final Gson gson = new Gson();


    @Value("${camunda.api.base-url}")
    private String camundaBaseUrl;

    @Autowired
    public CamundaService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String startProcessInstanceByName(String processName) {
        final String url = camundaBaseUrl + String.format("process-definition/key/%s/start", processName);

        final HttpEntity request = CamundaUtils.createRequestNoBody();
        final ProcessInstanceDto processInstanceDto =
                restTemplate.postForEntity(url, request, ProcessInstanceDto.class).getBody();
        if (processInstanceDto == null) {
            throw new CamundaException("Process instance failed to start!");
        }
        return processInstanceDto.getId();
    }

    public String startProcessInstanceByNameWithInitiator(String processName, String initiatorName) {
        final String url = camundaBaseUrl + String.format("process-definition/key/%s/start", processName);

        final HttpEntity request = CamundaUtils.createRequestBody(
                "{\"variables\":{\"author\":{\"value\":\"" + initiatorName + "\"}}}");
        final ProcessInstanceDto processInstanceDto =
                restTemplate.postForEntity(url, request, ProcessInstanceDto.class).getBody();
        if (processInstanceDto == null) {
            throw new CamundaException("Process instance failed to start!");
        }
        return processInstanceDto.getId();
    }


    public String getActiveTaskIdByProcessInstanceId(String processInstanceId) {

        doesProcessInstanceExistId(processInstanceId);
        final String url = camundaBaseUrl + "task?processInstanceId=" + processInstanceId;
        final TaskDto[] taskDtos =
                restTemplate.getForEntity(url, TaskDto[].class).getBody();
        if (taskDtos == null || taskDtos.length < 1) {
            throw new CamundaException("No task found for the given process instance id.");
        }
        return taskDtos[0].getId();
    }

    public boolean isProcessInstanceActive(String processInstanceId) {
        final String url = camundaBaseUrl + "task?processInstanceId=" + processInstanceId;
        final TaskDto[] taskDtos =
                restTemplate.getForEntity(url, TaskDto[].class).getBody();
        return taskDtos != null && taskDtos.length != 0;
    }

    public String getFormInHtml(String taskId) {
        final String url = camundaBaseUrl + String.format("task/%s/rendered-form", taskId);
        return restTemplate.getForEntity(url, String.class).getBody();
    }

    public void submitTaskForm(String taskId, Map<String, Object> map) {
        final String url = camundaBaseUrl + String.format("task/%s/submit-form", taskId);
        final String submitForm = CamundaUtils.createSubmitTaskFormObject(map);
        final HttpEntity request = CamundaUtils.createRequestBody(submitForm);
        restTemplate.postForEntity(url, request, Object.class);
    }


    public void setVariable(String processInstanceId, String name, Object value) {
        final String url = camundaBaseUrl + String.format("process-instance/%s/variables/%s", processInstanceId, name);
        final HttpEntity request = CamundaUtils.createRequestBody("{\"value\":\"" + value.toString() + "\"}");
        restTemplate.put(url, request);
    }

    public void setListVariable(String processInstanceId, String name, Object value) {
        final String url = camundaBaseUrl + String.format("process-instance/%s/variables/%s", processInstanceId, name);
        final HttpEntity request = CamundaUtils.createRequestBody("{\"value\":\"" + value.toString() + "\"}");
        restTemplate.put(url, request);
    }

    public List<String> getListVariable(String variableName, String processInstanceId) {
        final String url = camundaBaseUrl +
                String.format("process-instance/%s/variables/%s?deserializeValue=false", processInstanceId, variableName);
        final VariableValueDto valueDto =
                restTemplate.getForEntity(url, VariableValueDto.class).getBody();
        final String val = ((String) valueDto.getValue()).substring(1, ((String) valueDto.getValue()).length() - 1);
        if (val.equals("")) return new ArrayList<>();
        return Arrays.asList(val.split(",")).stream().map(String::trim).collect(Collectors.toList());
    }

    public HashMap<String, Object> getProcessVariable(String variableName, String processInstanceId) {
        final String url = camundaBaseUrl +
                String.format("process-instance/%s/variables/%s?deserializeValue=false", processInstanceId, variableName);
        final VariableValueDto valueDto =
                restTemplate.getForEntity(url, VariableValueDto.class).getBody();
        String stringified = (String) valueDto.getValue();
        return gson.fromJson(stringified, HashMap.class);
    }

    public Object getVariable(String variableName, String processInstanceId) {
        final String url = camundaBaseUrl +
                String.format("process-instance/%s/variables/%s?deserializeValue=false", processInstanceId, variableName);
        final VariableValueDto valueDto =
                restTemplate.getForEntity(url, VariableValueDto.class).getBody();
        return valueDto.getValue();
    }

    public void completeTask(String taskId) {
        final String url = camundaBaseUrl + String.format("/task/%s/complete", taskId);
        final HttpEntity request = CamundaUtils.createRequestNoBody();
        restTemplate.postForEntity(url, request, Object.class);
    }

    public void completeTaskWithVariable(String taskId, String variableName, String variableValue) {
        final String url = camundaBaseUrl + String.format("/task/%s/complete", taskId);
        final HttpEntity request = CamundaUtils.
                createRequestBody("{\"variables\":{\"" + variableName + "\":{\"value\":\"" + variableValue + "\"}}}");
        restTemplate.postForEntity(url, request, Object.class);
    }

    public TaskDto[] getAllTasksByAssignee(String assignee) {
        final String url = camundaBaseUrl +
                String.format("task/?assignee=%s&active=true", assignee);
        return restTemplate.getForEntity(url, TaskDto[].class).getBody();
    }

    private void doesProcessInstanceExistId(String processInstanceId) {

        final String url = camundaBaseUrl + "process-instance/" + processInstanceId;
        try {
            final ProcessInstanceDto processInstanceDto =
                    restTemplate.getForEntity(url, ProcessInstanceDto.class).getBody();

            if (processInstanceDto == null) {
                throw new CamundaException("Process instance with id " + processInstanceId + " does not exist!");
            }
        } catch (HttpClientErrorException e) {
            throw new CamundaException("Process instance with id " + processInstanceId + " does not exist!");
        }
    }
}
