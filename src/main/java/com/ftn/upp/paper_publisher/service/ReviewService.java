package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Review;
import com.ftn.upp.paper_publisher.domain.ReviewAdviceType;
import com.ftn.upp.paper_publisher.domain.ScientificPaper;
import com.ftn.upp.paper_publisher.domain.auth.Account;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.repository.ReviewDao;
import com.ftn.upp.paper_publisher.security.UserService;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class ReviewService {

    private final ReviewDao reviewDao;
    private final CamundaService camundaService;
    private final EmailService emailService;
    private final ScientificPaperService scientificPaperService;
    private final UserService userService;
    private final AccountService accountService;

    @Autowired
    public ReviewService(ReviewDao reviewDao, CamundaService camundaService,
                         EmailService emailService, ScientificPaperService scientificPaperService,
                         AccountService accountService, UserService userService) {
        this.reviewDao = reviewDao;
        this.camundaService = camundaService;
        this.emailService = emailService;
        this.scientificPaperService = scientificPaperService;
        this.accountService = accountService;
        this.userService = userService;
    }

    public List<Review> getAllForPaper(Long paperId) {
        return reviewDao.getAllByScientificPaperId(paperId);
    }
    public List<Review> getAllByProcessInstanceId(String processInstanceId) {
        return reviewDao.getAllByProcessInstanceId(processInstanceId);
    }

    public void submittedReviewNotify(String editor) {
        emailService.sendMessage(editor,
                EmailService.REVIEW_SUBMITTED_TITLE, EmailService.REVIEW_SUBMITTED_MESSAGE);
    }

    public void reviewTimeIsUp(String editorUsername) {
        emailService.sendMessage(editorUsername,
                EmailService.REVIEWER_IS_LATE_TITLE, EmailService.REVIEWER_IS_LATE_MESSAGE);
    }


    public UserForm getForm(String processInstanceId) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        return new UserForm(processInstanceId, camundaService.getFormInHtml(taskId));
    }

    public void submitForm(String processInstanceId, HashMap<String, Object> formSubmission) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        saveReview(processInstanceId, formSubmission);
        camundaService.submitTaskForm(taskId, formSubmission);
    }

    private void saveReview(String processInstanceId, HashMap<String, Object> formSubmission) {

        final String paperId = (String) camundaService.getVariable("paperId", processInstanceId);

        final ScientificPaper scientificPaper = scientificPaperService.getById(Long.valueOf(paperId));
        final Account reviewer = accountService.findByUsername(userService.getCurrentUsername());

        final Review review = new Review();
        review.setProcessInstanceId(processInstanceId);
        review.setScientificPaper(scientificPaper);
        review.setReviewer(reviewer);

        if (formSubmission.get("comments") != null) {
            review.setComments((String) formSubmission.get("comments"));
        }
        if (formSubmission.get("additionalComment") != null) {
            review.setComments((String) formSubmission.get("additionalComment"));
        }
        if (formSubmission.get("advice") != null) {
            review.setReviewAdviceType(ReviewAdviceType.valueOf((String) formSubmission.get("advice")));
        }
        if (formSubmission.get("additionalAdvice") != null) {
            review.setReviewAdviceType(ReviewAdviceType.valueOf((String) formSubmission.get("additionalAdvice")));
        }
        if (formSubmission.get("commentsForEditors") != null) {
            review.setCommentsForEditors((String) formSubmission.get("commentsForEditors"));
        }
        if (formSubmission.get("additionalCommentsForEditors") != null) {
            review.setCommentsForEditors((String) formSubmission.get("additionalCommentsForEditors"));
        }

        reviewDao.save(review);
    }
}
