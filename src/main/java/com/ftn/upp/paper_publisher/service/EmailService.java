package com.ftn.upp.paper_publisher.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    private final JavaMailSender emailSender;

    @Autowired
    public EmailService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public static final String SCIENTIFIC_PAPER_EMAIL = "scientific.paper.publisher@gmail.com";

    public static final String SUBMISSION_TITLE_AUTHOR = "Submission received";
    public static final String SUBMISSION_MESSAGE_AUTHOR = "Respected sir/madam, " +
            "\n\nYour submission to ScientificPaperPublisher has been received. " +
            "Thank you for your interest!\n\nKind regards!";

    public static final String SUBMISSION_TITLE = "Submission received";
    public static final String SUBMISSION_MESSAGE = "Respected sir/madam, " +
            "\n\nNew submission to ScientificPaperPublisher has been received. " +
            "\n\nKind regards!";

    public static final String SELECT_REVIEWERS_TITLE = "Submission received, choose reviewers";
    public static final String SELECT_REVIEWERS_MESSAGE = "Respected sir/madam, " +
            "\n\nPlease, select at least 2 reviewers for submitted paper." +
            "\n\nKind regards!";

    public static final String FORMATTING_NEEDED_TITLE = "Paper format";
    public static final String FORMATTING_NEEDED_MESSAGE = "Respected sir/madam, " +
            "\n\nPlease, reformat submitted paper." +
            "\n\nKind regards!";

    public static final String REVISION_NEEDED_TITLE = "Paper format";
    public static final String REVISION_NEEDED_MESSAGE = "Respected sir/madam, " +
            "\n\nPlease, send us a revision of the submitted paper according to reviews." +
            "\n\nKind regards!";

    public static final String REVISION_SUBMISSION_TITLE = "Submission received";
    public static final String REVISION_SUBMISSION_MESSAGE = "Respected sir/madam, " +
            "\n\nRevision submitted to ScientificPaperPublisher has been received. " +
            "\n\nKind regards!";

    public static final String REVIEW_TITLE = "Review";
    public static final String REVIEW_MESSAGE = "Respected sir/madam, " +
            "\n\n I would like you to comments paper and give your professional opinion." +
            " Thank you in advance. \n\nKind regards!\n\n";

    public static final String REVIEW_SUBMITTED_TITLE = "Review received";
    public static final String REVIEW_SUBMITTED_MESSAGE = "Respected sir/madam, " +
            "\n\nReview has been submitted. Thank you very much for your evaluation!\n\nKind regards!";

    public static final String REVIEWER_IS_LATE_TITLE = "Reviewing time is up";
    public static final String REVIEWER_IS_LATE_MESSAGE = "Respected sir/madam, " +
            "\n\nReviewers are late!\n\nKind regards!";

    public static final String PUBLISH_TITLE = "Paper published";
    public static final String PUBLISH_MESSAGE = "Respected sir/madam, " +
            "\n\nCongratulations: your paper has been published! " +
            "Thank you for your interest. We are looking forward to further collaboration!\n\nKind regards!";

    public static final String REJECT_TITLE = "Paper rejected";
    public static final String REJECT_MESSAGE = "Respected sir/madam, " +
            "\n\nWe are sorry to inform you that your paper is rejected due to %s reasons. " +
            "Thank you for your interest. We are looking forward to further collaboration!\n\nKind regards!";


    public void sendMessage(
            String to, String subject, String text) {

        final SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
        // TODO: COMMENT OUT SENDING MEJL
    }
}