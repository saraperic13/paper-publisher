package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.exception.UploadFailed;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class StorageService {

    //    @Value("${upload.path}")
    private final String dirPath = "\\src\\main\\resources\\paper-uploads";

    private final String path = System.getProperty("user.dir") + dirPath;
    private Path rootLocation = Paths.get(this.path);


    public String store(MultipartFile file) {
        final String fileName = generateFileName();
        try {
            Files.copy(file.getInputStream(), this.rootLocation.resolve(fileName + ".pdf"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new UploadFailed("Uploading paper failed!");
        }
        return fileName;
    }

    public String getFullPath(String fileName) {
        return path + "\\" + fileName + ".pdf";
    }

    private String generateFileName() {
        return UUID.randomUUID().toString();
    }

}
