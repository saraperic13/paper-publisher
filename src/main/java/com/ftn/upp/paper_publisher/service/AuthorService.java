package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Author;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.AuthorDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {

    private final AuthorDao authorDao;

    @Autowired
    public AuthorService(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    public Author save(Author author) {
        return authorDao.save(author);
    }

    public Author getByUsername(String username) {
        return this.authorDao.getByUsername(username).orElseThrow(()
                -> new EntityDoesNotExistException("Author with given username does not exist!"));
    }
}
