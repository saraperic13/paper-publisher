package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.ScientificPaperVersion;
import com.ftn.upp.paper_publisher.repository.ScientificPaperVersionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScientificPaperVersionService {

    private final ScientificPaperVersionDao scientificPaperVersionDao;

    @Autowired
    public ScientificPaperVersionService(ScientificPaperVersionDao scientificPaperVersionDao) {
        this.scientificPaperVersionDao = scientificPaperVersionDao;
    }

    public ScientificPaperVersion save(ScientificPaperVersion version) {
        return scientificPaperVersionDao.save(version);
    }
}
