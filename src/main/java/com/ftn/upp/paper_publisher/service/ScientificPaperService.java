package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.*;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import com.ftn.upp.paper_publisher.exception.CamundaException;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.ScientificPaperDao;
import com.ftn.upp.paper_publisher.security.UserService;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class ScientificPaperService {

    private final CoauthorService coauthorService;

    private final CamundaService camundaService;

    private final ScientificPaperDao scientificPaperDao;

    private final UserService userService;

    private final AuthorService authorService;

    private final ScientificPaperVersionService versionService;

    private final EmailService emailService;

    private final JournalService journalService;

    @Autowired
    public ScientificPaperService(ScientificPaperDao scientificPaperDao,
                                  CamundaService camundaService,
                                  CoauthorService coauthorService,
                                  UserService userService,
                                  AuthorService authorService,
                                  ScientificPaperVersionService versionService,
                                  EmailService emailService,
                                  JournalService journalService) {
        this.scientificPaperDao = scientificPaperDao;
        this.coauthorService = coauthorService;
        this.camundaService = camundaService;
        this.userService = userService;
        this.authorService = authorService;
        this.versionService = versionService;
        this.emailService = emailService;
        this.journalService = journalService;
    }


    public void submitFormAndSavePaper(String processInstanceId, HashMap<String, Object> paper, String filePath) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        try {
            final String authorUsername =
                    (String) camundaService.getVariable("author", processInstanceId);
            final String issn =
                    (String) camundaService.getVariable("issn", processInstanceId);

            final Long paperId = createAndSavePaper(paper, filePath, authorUsername, processInstanceId, issn);
            camundaService.submitTaskForm(taskId, paper);
            camundaService.setVariable(processInstanceId, "paperId", paperId);
        } catch (Exception e) {
            throw new CamundaException("Invalid input data");
        }
    }

    public void submitRevision(String processInstanceId,String filePath,
                               HashMap<String, Object> formSubmission) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        createNewPaperVersion(processInstanceId, filePath);
        camundaService.submitTaskForm(taskId, formSubmission);
    }

    public void submitReformattedPaper(String processInstanceId, String filePath) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        createNewPaperVersion(processInstanceId, filePath);
        camundaService.completeTask(taskId);
    }

    public void revisionSubmitted(String editorUsername) {
        emailService.sendMessage(editorUsername,
                EmailService.REVISION_SUBMISSION_TITLE, EmailService.REVISION_SUBMISSION_MESSAGE);
    }

    public void rejectPaper(Long paperId, String author, String reason) {
        final ScientificPaper paper = getById(paperId);
        paper.setState(ScientificPaperState.REJECTED);
        scientificPaperDao.save(paper);

        emailService.sendMessage(author,
                EmailService.REJECT_TITLE, String.format(EmailService.REJECT_MESSAGE, reason));
    }

    public void acceptPaper(Long paperId, String authorEmail) {
        final ScientificPaper paper = getById(paperId);
        paper.setPrice(20.0);
        paper.setState(ScientificPaperState.ACCEPTED);
        scientificPaperDao.save(paper);

        emailService.sendMessage(authorEmail,
                EmailService.PUBLISH_TITLE, EmailService.PUBLISH_MESSAGE);
    }

    public void paperRevisionNeeded(Long paperId, String authorEmail) {
        final ScientificPaper paper = getById(paperId);
        paper.setState(ScientificPaperState.ON_REVISION);
        scientificPaperDao.save(paper);

        emailService.sendMessage(authorEmail,
                EmailService.REVISION_NEEDED_TITLE, EmailService.REVISION_NEEDED_MESSAGE);
    }

    public void paperFormatingNeeded(Long paperId, String author) {
        final ScientificPaper paper = getById(paperId);
        paper.setState(ScientificPaperState.ON_REVISION);
        scientificPaperDao.save(paper);

        emailService.sendMessage(author, EmailService.FORMATTING_NEEDED_TITLE, EmailService.FORMATTING_NEEDED_MESSAGE);
    }

    public InputStreamResource getPaperPdf(Long paperId) {
        final ScientificPaper paper = getById(paperId);
        final ScientificPaperVersion version = paper.getVersions().get(paper.getVersions().size() - 1);

        try {
            return new InputStreamResource(new FileInputStream(version.getFilePath()));
        } catch (FileNotFoundException e) {
            throw new EntityDoesNotExistException("Pdf file does not exist!");
        }
    }

    public ScientificPaper getById(Long id) {
        return this.scientificPaperDao.findById(id).orElseThrow(()
                -> new EntityDoesNotExistException("Scientific paper with required id does not exist!"));
    }

    public List<ScientificPaper> getAll(Long journalId) {
        return scientificPaperDao.findAllByJournalIdAndState(journalId, ScientificPaperState.ACCEPTED);
    }

    public ScientificPaper getByProcessInstanceId(String processInstanceId) {
        return scientificPaperDao.findByProcessInstanceId(processInstanceId).orElseThrow(() ->
                new EntityDoesNotExistException("Scientific paper with given process instance id does not exist!"));
    }

    private Long createAndSavePaper(HashMap<String, Object> paper, String filePath, String authorUsername,
                                    String processInstanceId, String issn) {
        ScientificPaper scientificPaper = new ScientificPaper();
        scientificPaper.setState(ScientificPaperState.SUBMITTED);
        scientificPaper.setProcessInstanceId(processInstanceId);
        scientificPaper.setJournal(journalService.getByIssn(issn));
        final Author author = authorService.getByUsername(authorUsername);
        scientificPaper.setAuthor(author);

        if (paper.get("title") != null) {
            scientificPaper.setName((String) paper.get("title"));
        }
        if (paper.get("keywords") != null) {
            scientificPaper.setKeywords(new ArrayList<>(Arrays.asList(((String) paper.get("keywords")).split(","))));
        }
        if (paper.get("abstract") != null) {
            scientificPaper.setPaperAbstract((String) paper.get("abstract"));
        }
        if (paper.get("coauthors") != null) {
            List<Coauthor> coauthors = parseCoauthors(paper);
            scientificPaper.setCoauthors(coauthors);
        }
        if (paper.get("scientificField") != null) {
            scientificPaper.setScientificField(ScientificField.valueOf((String) paper.get("scientificField")));
        }

        ScientificPaperVersion version = new ScientificPaperVersion();
        version.setFilePath(filePath);
        version = versionService.save(version);

        scientificPaper.getVersions().add(version);
        scientificPaper = scientificPaperDao.save(scientificPaper);

        author.getScientificPapers().add(scientificPaper);
        authorService.save(author);

        return scientificPaper.getId();
    }

    private List<Coauthor> parseCoauthors(HashMap<String, Object> paper) {
        final List<Coauthor> coauthors = new ArrayList<>();
        final String[] coauthorsString = ((String) paper.get("coauthors")).split(";");

        for (String aCoauthorsString : coauthorsString) {
            final Coauthor coauthor = new Coauthor();
            coauthor.setName(aCoauthorsString);
            coauthors.add(coauthor);
        }

        final String addressesString = (String) paper.get("coauthorsAddresses");
        if (addressesString != null) {
            final String[] addresses = addressesString.split(";");
            for (int i = 0; i < addresses.length; i++) {
                coauthors.get(i).setAddress(addresses[i]);
            }
        }
        final String emailsString = (String) paper.get("coauthorsEmails");
        if (emailsString != null) {
            final String[] emails = emailsString.split(";");
            for (int i = 0; i < emails.length; i++) {
                coauthors.get(i).setEmail(emails[i]);
            }
        }
        return coauthorService.saveAll(coauthors);
    }

    private void createNewPaperVersion(String processInstanceId, String filePath) {
        ScientificPaperVersion version = new ScientificPaperVersion();
        version.setFilePath(filePath);
        version = versionService.save(version);

        final ScientificPaper paper = getByProcessInstanceId(processInstanceId);
        paper.getVersions().add(version);
        scientificPaperDao.save(paper);
    }

    public UserForm getForm(String processInstanceId) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        return new UserForm(processInstanceId, camundaService.getFormInHtml(taskId));
    }
}
