package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.controller.dto.StatusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PaymentService {

    private final MembershipService membershipService;
    private final RestTemplate restTemplate;

    @Autowired
    public PaymentService(MembershipService membershipService,
                          RestTemplate restTemplate) {
        this.membershipService = membershipService;
        this.restTemplate = restTemplate;
    }

    public boolean checkPayment(String issn, String username) {
        String status = "pending";
        while (status.equals("pending")) {
            final StatusDto statusDto = restTemplate.
                    getForEntity("http://localhost:8080/api/paypal/check_payment", StatusDto.class).getBody();
            status = statusDto.getStatus();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (status.equals("success")) {
            this.membershipService.createNewMembership(issn, username);
            return true;
        }
        return false;
    }
}
