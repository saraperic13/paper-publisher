package com.ftn.upp.paper_publisher.service;


import com.ftn.upp.paper_publisher.domain.auth.Account;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.AccountDao;
import com.ftn.upp.paper_publisher.security.TokenUtils;
import com.ftn.upp.paper_publisher.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private final AccountDao accountDao;

    private final AuthenticationManager authenticationManager;

    private final UserDetailsServiceImpl userDetailsService;

    private final TokenUtils tokenUtils;

    @Autowired
    public AccountService(AccountDao accountDao,
                          AuthenticationManager authenticationManager,
                          UserDetailsServiceImpl userDetailsService,
                          TokenUtils tokenUtils) {
        this.accountDao = accountDao;
        this.authenticationManager = authenticationManager;
        this.tokenUtils = tokenUtils;
        this.userDetailsService = userDetailsService;
    }

    public Account findByUsername(String username) {
        return accountDao.findByUsername(username).orElseThrow(() ->
                new EntityDoesNotExistException("User with specified username does not exist!"));
    }

    void save(Account account) {
        accountDao.save(account);
    }

    public String login(String username, String password) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            final UserDetails details = userDetailsService.loadUserByUsername(username);
            return tokenUtils.generateToken(details);

        } catch (Exception ex) {
            throw new BadCredentialsException("Wrong username/password");
        }
    }
}
