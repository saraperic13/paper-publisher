package com.ftn.upp.paper_publisher.service.init;

import com.ftn.upp.paper_publisher.domain.*;
import com.ftn.upp.paper_publisher.domain.auth.Role;
import com.ftn.upp.paper_publisher.domain.auth.RoleType;
import com.ftn.upp.paper_publisher.domain.enumeration.JournalType;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import com.ftn.upp.paper_publisher.repository.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class InitService {

    private final JournalDao journalDao;

    private final ScientificPaperDao scientificPaperDao;
    private final EditorDao editorDao;
    private final RoleDao roleDao;
    private final ReviewerDao reviewerDao;
    private final EditorialBoardDao editorialBoardDao;
    private final AuthorDao authorDao;
    private final ScientificPaperVersionDao scientificPaperVersionDao;
    private final BCryptPasswordEncoder passwordEncoder;
    private final MembershipDao membershipDao;

    public InitService(JournalDao journalDao, ScientificPaperDao scientificPaperDao,
                       EditorDao editorDao, RoleDao roleDao, ReviewerDao reviewerDao,
                       EditorialBoardDao editorialBoardDao, AuthorDao authorDao,
                       ScientificPaperVersionDao scientificPaperVersionDao,
                       MembershipDao membershipDao,
                       BCryptPasswordEncoder passwordEncoder) {
        this.journalDao = journalDao;
        this.scientificPaperDao = scientificPaperDao;
        this.editorDao = editorDao;
        this.roleDao = roleDao;
        this.reviewerDao = reviewerDao;
        this.editorialBoardDao = editorialBoardDao;
        this.authorDao = authorDao;
        this.scientificPaperVersionDao = scientificPaperVersionDao;
        this.membershipDao = membershipDao;
        this.passwordEncoder = passwordEncoder;
    }

    public void init() {
        if (!roleDao.findAll().isEmpty()) {
            return;
        }

        Role authorRole = new Role();
        authorRole.setRoleType(RoleType.AUTHOR);
        authorRole = roleDao.save(authorRole);

        Role chiefEditorRole = new Role();
        chiefEditorRole.setRoleType(RoleType.CHIEF_EDITOR);
        chiefEditorRole = roleDao.save(chiefEditorRole);

        Role editorRole = new Role();
        editorRole.setRoleType(RoleType.EDITOR);
        editorRole = roleDao.save(editorRole);

        Role reviewerRole = new Role();
        reviewerRole.setRoleType(RoleType.REVIEWER);
        reviewerRole = roleDao.save(reviewerRole);

        Editor chiefEditor = new Editor();
        chiefEditor.setUsername("editor1@gmail.com");
        chiefEditor.setFirstName("Editor");
        chiefEditor.setLastName("First");
        chiefEditor.getRoles().add(editorRole);
        chiefEditor.getRoles().add(chiefEditorRole);
        chiefEditor.setPassword(passwordEncoder.encode("editor123"));
        chiefEditor = editorDao.save(chiefEditor);

        Editor editor2 = new Editor();
        editor2.setUsername("editor2@gmail.com");
        editor2.setFirstName("Editor");
        editor2.setLastName("Second");
        editor2.getRoles().add(editorRole);
        editor2.getScientificField().add(ScientificField.COMPUTER_SCIENCE);
        editor2.setPassword(passwordEncoder.encode("editor123"));
        editor2 = editorDao.save(editor2);

        Reviewer reviewer1 = new Reviewer();
        reviewer1.setUsername("reviewer1@gmail.com");
        reviewer1.setFirstName("Reviewer");
        reviewer1.setLastName("First");
        reviewer1.getRoles().add(reviewerRole);
        reviewer1.setPassword(passwordEncoder.encode("reviewer123"));
        reviewer1.getScientificFields().add(ScientificField.COMPUTER_SCIENCE);
        reviewer1 = reviewerDao.save(reviewer1);

        Reviewer reviewer2 = new Reviewer();
        reviewer2.setUsername("reviewer2@gmail.com");
        reviewer2.setFirstName("Reviewer");
        reviewer2.setLastName("Second");
        reviewer2.getRoles().add(reviewerRole);
        reviewer2.setPassword(passwordEncoder.encode("reviewer123"));
        reviewer2.getScientificFields().add(ScientificField.COMPUTER_SCIENCE);
        reviewer2 = reviewerDao.save(reviewer2);

        Reviewer reviewer3 = new Reviewer();
        reviewer3.setUsername("reviewer3@gmail.com");
        reviewer3.setFirstName("Reviewer");
        reviewer3.setLastName("Third");
        reviewer3.getRoles().add(reviewerRole);
        reviewer3.getScientificFields().add(ScientificField.MEDICINE);
        reviewer3.setPassword(passwordEncoder.encode("reviewer123"));
        reviewer3 = reviewerDao.save(reviewer3);

        Journal journal1 = new Journal();
        journal1.setPaymentId("-1");
        journal1.setIssn("122121212L");
        journal1.setJournalType(JournalType.OPEN_ACCESS);
        journal1.setName("Scientific Point of View");
        journal1.getReviewers().add(reviewer1);
        journal1.getReviewers().add(reviewer2);
        journal1.getReviewers().add(reviewer3);
        journal1 = journalDao.save(journal1);

        EditorialBoard editorialBoard = new EditorialBoard();
        editorialBoard.setChiefEditor(chiefEditor);
        editorialBoard.getEditors().add(editor2);
        editorialBoard.setJournal(journal1);
        editorialBoard = editorialBoardDao.save(editorialBoard);
        journal1.setEditorialBoard(editorialBoard);
        journal1 = journalDao.save(journal1);
        chiefEditor.setEditorialBoard(editorialBoard);
        editor2.setEditorialBoard(editorialBoard);
        chiefEditor = editorDao.save(chiefEditor);
        editor2 = editorDao.save(editor2);

        Author sara = new Author();
        sara.setUsername("yingyang13sarah@gmail.com");
        sara.setFirstName("Sara");
        sara.setLastName("Sara");
        sara.getRoles().add(authorRole);
        sara.setPassword(passwordEncoder.encode("sara123"));
        sara = authorDao.save(sara);

        Author author1 = new Author();
        author1.setUsername("author1@gmail.com");
        author1.setFirstName("Author");
        author1.setLastName("First");
        author1.getRoles().add(authorRole);
        author1.setPassword(passwordEncoder.encode("author123"));
        author1 = authorDao.save(author1);

        Membership membership1 = new Membership();
        membership1.setAccount(author1);
        membership1.setJournal(journal1);
        membership1.setActiveUntil(LocalDateTime.now().plusWeeks(2));
        membership1 = membershipDao.save(membership1);

        ScientificPaperVersion version1 = new ScientificPaperVersion();
        version1.setFilePath("D:\\Documents\\V Godina\\UPP - Upravljanje Poslovnim Procesima\\paper_publisher\\src\\main\\resources\\paper-uploads\\paper1.pdf");
        version1 = scientificPaperVersionDao.save(version1);


        ScientificPaper paper1 = new ScientificPaper();
        paper1.setPrice(15.0);
        paper1.setAuthor(author1);
        paper1.setPaperAbstract("Paper abstract");
        paper1.getKeywords().add("Computer Science");
        paper1.getKeywords().add("Machine Learning");
        paper1.setName("Really cool paper");
        paper1.setJournal(journal1);
        paper1.getVersions().add(version1);
        paper1.setScientificField(ScientificField.COMPUTER_SCIENCE);
        paper1.setState(ScientificPaperState.ACCEPTED);
        paper1 = scientificPaperDao.save(paper1);

        author1.getScientificPapers().add(paper1);
        authorDao.save(author1);
    }

}
