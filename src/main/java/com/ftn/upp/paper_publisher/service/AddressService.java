package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Address;
import com.ftn.upp.paper_publisher.repository.AddressDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    private final AddressDao addressDao;

    @Autowired
    public AddressService(AddressDao addressDao) {
        this.addressDao = addressDao;
    }

    public Address save(Address address) {
        return addressDao.save(address);
    }
}
