package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Editor;
import com.ftn.upp.paper_publisher.domain.Journal;
import com.ftn.upp.paper_publisher.domain.Reviewer;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.ReviewerDao;
import com.ftn.upp.paper_publisher.security.UserService;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Service
public class ReviewerService {

    private final ReviewerDao reviewerDao;

    private final CamundaService camundaService;

    private final UserService userService;
    private final EditorService editorService;
    private final EditorialBoardService editorialBoardService;
    private final JournalService journalService;

    private final EmailService emailService;

    @Autowired
    public ReviewerService(ReviewerDao reviewerDao, CamundaService camundaService,
                           UserService userService, EditorService editorService,
                           EditorialBoardService editorialBoardService,
                           EmailService emailService, JournalService journalService) {
        this.reviewerDao = reviewerDao;
        this.camundaService = camundaService;
        this.userService = userService;
        this.editorService = editorService;
        this.emailService = emailService;
        this.editorialBoardService = editorialBoardService;
        this.journalService = journalService;
    }


    public List<Reviewer> getAllFromJournal() {
        final String username = userService.getCurrentUsername();
        final Editor editor = editorService.findByUsername(username);
        final Journal journal = editor.getEditorialBoard().getJournal();
        return journal.getReviewers();
    }

    public List<Reviewer> getReviewersFromScientificField(String processInstanceId) {
        final String scField = (String) camundaService.getVariable("scientificField", processInstanceId);
        final ScientificField scientificField = ScientificField.valueOf(scField);

        final List<Reviewer> allReviewers = getAllFromJournal();
        final List<Reviewer> filtered = new ArrayList<>();
        for (Reviewer reviewer : allReviewers) {
            if (reviewer.getScientificFields().contains(scientificField)) {
                filtered.add(reviewer);
            }
        }
        return filtered;
    }

    public List<Reviewer> getUnassignedReviewers(String processInstanceId) {
        List<String> reviewers =
                camundaService.getListVariable("reviewersString", processInstanceId);

        final String issn = (String) camundaService.getVariable("issn", processInstanceId);
        final Journal journal = journalService.getByIssn(issn);

        final List<Reviewer> unassigned = new ArrayList<>();
        for (Reviewer reviewer : journal.getReviewers()) {
            if (!reviewers.contains(reviewer.getUsername())) {
                unassigned.add(reviewer);
            }
        }
        return unassigned;
    }

    public void submitSelectReviewersForm(String processInstanceId, List<String> reviewers) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        camundaService.setListVariable(processInstanceId, "reviewersString", reviewers);
        camundaService.completeTask(taskId);
    }


    public void assignAdditionalReviewer(String processInstanceId, String username) {
        final Reviewer reviewer = getByUsername(username);
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        camundaService.setVariable(processInstanceId, "additionalReviewer", reviewer.getUsername());
        camundaService.completeTask(taskId);

        emailService.sendMessage(reviewer.getUsername(), EmailService.REVIEW_TITLE, EmailService.REVIEW_MESSAGE);
    }

    public void notifyReviewers(List<String> reviewers) {
        reviewers.stream().forEach(reviewer
                -> emailService.sendMessage(reviewer, EmailService.REVIEW_TITLE, EmailService.REVIEW_MESSAGE));
    }

    public void assignReviewToChiefEditor(String processInstanceId, String issn, DelegateExecution execution) {
        List<String> reviewers =
                camundaService.getListVariable("reviewersString", processInstanceId);

        if (reviewers.size()== 0) {
            final Journal journal = journalService.getByIssn(issn);
            final Editor chiefEditor = journal.getEditorialBoard().getChiefEditor();
            reviewers = Collections.singletonList(chiefEditor.getUsername());
            emailService.sendMessage(chiefEditor.getUsername(), EmailService.REVIEW_TITLE, EmailService.REVIEW_MESSAGE);
        }
        execution.setVariable("rewNum", reviewers.size());
        System.out.println(execution.getVariable("rewNum"));
        execution.setVariable("reviewers", reviewers);
    }

    public Reviewer getById(Long id) {
        return reviewerDao.findById(id).orElseThrow(()
                -> new EntityDoesNotExistException("Reviewer with given id not found!"));
    }

    public Reviewer getByUsername(String username) {
        return reviewerDao.getByUsername(username).orElseThrow(()
                -> new EntityDoesNotExistException("Reviewer with given id not found!"));
    }

}
