package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Journal;
import com.ftn.upp.paper_publisher.domain.Membership;
import com.ftn.upp.paper_publisher.domain.auth.Account;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.repository.MembershipDao;
import com.ftn.upp.paper_publisher.security.UserService;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;

@Service
public class MembershipService {

    private final MembershipDao membershipDao;
    private final CamundaService camundaService;
    private final AccountService accountService;
    public final JournalService journalService;
    private final UserService userService;


    @Autowired
    public MembershipService(MembershipDao membershipDao,
                             CamundaService camundaService,
                             AccountService accountService,
                             JournalService journalService,
                             UserService userService) {
        this.membershipDao = membershipDao;
        this.camundaService = camundaService;
        this.userService = userService;
        this.accountService = accountService;
        this.journalService = journalService;
    }

    public Membership save(Membership membership) {
        return membershipDao.save(membership);
    }

    private Membership getByJournalIssnAndAccountUsername(String issn, String username) {
        return membershipDao.getByJournalIssnAndAccountUsername(issn, username);
    }

    public void createNewMembership(String issn, String username) {
        final Account account = accountService.findByUsername(username);
        final Journal journal = journalService.getByIssn(issn);
        final Membership membership = new Membership();
        membership.setJournal(journal);
        membership.setAccount(account);
        membership.setActiveUntil(LocalDateTime.now().plusMonths(1));
        save(membership);
    }

    public boolean isMembershipActive(String journalISSN, String username) {

        final Membership membership = getByJournalIssnAndAccountUsername(journalISSN, username);
        return membership != null && membership.getActiveUntil().isAfter(LocalDateTime.now());
    }

    public UserForm getMembershipForm(String processInstanceId) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        return new UserForm(processInstanceId, camundaService.getFormInHtml(taskId));
    }

    public void submitForm(String processInstanceId, HashMap<String, Object> map) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        camundaService.submitTaskForm(taskId, map);
    }
}
