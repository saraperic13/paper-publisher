package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.auth.Role;
import com.ftn.upp.paper_publisher.domain.auth.RoleType;
import com.ftn.upp.paper_publisher.repository.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private final RoleDao roleDao;

    @Autowired
    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public Role save(Role role) {
        return roleDao.save(role);
    }

    public Role getByRoleType(RoleType roleType) {
        return roleDao.getByRoleType(roleType);
    }
}
