package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Coauthor;
import com.ftn.upp.paper_publisher.repository.CoauthorDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoauthorService {

    private final CoauthorDao coauthorDao;

    @Autowired
    public CoauthorService(CoauthorDao coauthorDao) {
        this.coauthorDao = coauthorDao;
    }

    public Coauthor save(Coauthor coauthor) {
        return coauthorDao.save(coauthor);
    }

    public List<Coauthor> saveAll(List<Coauthor> coauthors) {
        return coauthorDao.saveAll(coauthors);
    }
}
