package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Address;
import com.ftn.upp.paper_publisher.domain.Author;
import com.ftn.upp.paper_publisher.domain.auth.RoleType;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.exception.CamundaException;
import com.ftn.upp.paper_publisher.exception.DuplicateEmailException;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import com.ftn.upp.paper_publisher.utils.HtmlUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class RegistrationService {

    private final AccountService accountService;
    private final CamundaService camundaService;
    private final RoleService roleService;
    private final AddressService addressService;

    private final BCryptPasswordEncoder passwordEncoder;

    private final String REGISTRATION_FORM_VARIABLE_NAME = "registration";
    private final String REGISTRATION_PROCESS_NAME = "registration_process";

    @Autowired
    public RegistrationService(AccountService accountService,
                               CamundaService camundaService,
                               AddressService addressService,
                               RoleService roleService, BCryptPasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.roleService = roleService;
        this.camundaService = camundaService;
        this.addressService = addressService;
        this.passwordEncoder = passwordEncoder;
    }

    public UserForm getRegistrationForm() {

        final String processInstanceId = camundaService.startProcessInstanceByName(REGISTRATION_PROCESS_NAME);
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        return new UserForm(processInstanceId, camundaService.getFormInHtml(taskId));
    }

    public void register(String processInstanceId, HashMap<String, Object> map) {

        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        camundaService.setVariable(processInstanceId, REGISTRATION_FORM_VARIABLE_NAME, map);
        camundaService.submitTaskForm(taskId, map);

        boolean emailExists = camundaService.isProcessInstanceActive(processInstanceId);
        if (emailExists) {
            throw new DuplicateEmailException("Account with the same username already exists!");
        }
    }

    public boolean validate(DelegateExecution execution) throws Exception {

        final HashMap<String, Object> registration =
                camundaService.getProcessVariable(REGISTRATION_FORM_VARIABLE_NAME, execution.getProcessInstanceId());

        if (registration == null) {
            throw new CamundaException("Registration process variable does not exist");
        }

        for (String formField : registration.keySet()) {
            if (formField.equals("email")) {
                try {
                    accountService.findByUsername((String) registration.get(formField));
                    return false;
                } catch (EntityDoesNotExistException e) {
                    return true;
                }
            }
        }
        return false;
    }

    public void registerUser(DelegateExecution execution) throws Exception {

        final Author author = new Author();

        final HashMap<String, Object> registration =
                camundaService.getProcessVariable(REGISTRATION_FORM_VARIABLE_NAME, execution.getProcessInstanceId());

        if (registration == null) {
            throw new CamundaException("Registration process variable does not exist");
        }
        Address address = new Address();
        for (String formField : registration.keySet()) {
            if (formField.equals("email")) {
                author.setUsername((String) registration.get(formField));
            }
            if (formField.equals("password")) {
                final String password = (String) registration.get(formField);
                author.setPassword(passwordEncoder.encode(password));
            }
            if (formField.equals("firstName")) {
                author.setFirstName((String) registration.get(formField));
            }
            if (formField.equals("lastName")) {
                author.setLastName((String) registration.get(formField));
            }
            if (formField.equals("city")) {
                address.setCity((String) registration.get(formField));
            }
            if (formField.equals("country")) {
                address.setCountry((String) registration.get(formField));
            }
        }

        address = addressService.save(address);
        author.setAddress(address);
        author.getRoles().add(roleService.getByRoleType(RoleType.AUTHOR));
        accountService.save(author);
    }
}
