package com.ftn.upp.paper_publisher.service.camunda;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Map;

public class CamundaUtils {

    static HttpEntity createRequestNoBody() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>("{}", headers);
    }

    static HttpEntity createRequestBody(Object body) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(body, headers);
    }

    static String createSubmitTaskFormObject(Map<String, Object> map) {
        StringBuilder json = new StringBuilder("{ \"variables\": {");
        for (String key : map.keySet()) {
            json.append("\"").append(key).append("\":{\"value\":\"").append(map.get(key)).append("\"},");
        }
        json = new StringBuilder(json.substring(0, json.length() - 1));
        json.append("}}");
        return json.toString();
    }
}
