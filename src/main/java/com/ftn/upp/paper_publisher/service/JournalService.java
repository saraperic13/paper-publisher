package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Editor;
import com.ftn.upp.paper_publisher.domain.Journal;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.domain.enumeration.JournalType;
import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.JournalDao;
import com.ftn.upp.paper_publisher.security.UserService;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;

@Service
public class JournalService {

    private final String PAPER_PUBLISHING_PROCESS_NAME = "paper_publishing_process";

    private final CamundaService camundaService;
    private final EmailService emailService;
    private final JournalDao journalDao;
    private final UserService userService;

    @Autowired
    public JournalService(CamundaService camundaService, EmailService emailService,
                          UserService userService, JournalDao journalDao) {
        this.journalDao = journalDao;
        this.camundaService = camundaService;
        this.emailService = emailService;
        this.userService = userService;
    }

    public List<Journal> getAll() {
        return journalDao.findAll();
    }

    public Journal getById(Long id) {
        return journalDao.getById(id).orElseThrow(()
                -> new EntityDoesNotExistException(String.format("Journal with given id %s does not exist", id)));
    }

    public Journal getByIssn(String issn) {
        return journalDao.getByIssn(issn).orElseThrow(()
                -> new EntityDoesNotExistException(String.format("Journal with given issn %s does not exist", issn)));
    }

    public UserForm getChooseJournalForm() {
        final String currentUser = userService.getCurrentUsername();
        final String processInstanceId =
                camundaService.startProcessInstanceByNameWithInitiator(PAPER_PUBLISHING_PROCESS_NAME, currentUser);
        camundaService.setVariable(processInstanceId, "processInstanceId", processInstanceId);

        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        return new UserForm(processInstanceId, camundaService.getFormInHtml(taskId));
    }

    public void submitChooseJournalForm(String processInstanceId, String issn) {

        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        try {
            camundaService.completeTaskWithVariable(taskId, "issn", issn);
        } catch (HttpServerErrorException e) {
            throw new EntityDoesNotExistException("Given journal does not exist!");
        }
    }

    public boolean isJournalOpenAccess(String issn) {
        final Journal journal = getByIssn(issn);
        return journal.getJournalType().equals(JournalType.OPEN_ACCESS);
    }

    public void paperSubmitted(String processInstanceId, String issn, String author, DelegateExecution execution) {
        final Journal journal = getByIssn(issn);
        final Editor chiefEditor = journal.getEditorialBoard().getChiefEditor();
        execution.setVariable("chiefEditor", chiefEditor.getUsername());
        this.emailService.sendMessage(chiefEditor.getUsername(),
                EmailService.SUBMISSION_TITLE, EmailService.SUBMISSION_MESSAGE);
        this.emailService.sendMessage(author,
                EmailService.SUBMISSION_TITLE_AUTHOR, EmailService.SUBMISSION_MESSAGE_AUTHOR);
    }

    public boolean assignEditor(String issn, String field, DelegateExecution execution) {
        final ScientificField scientificField = ScientificField.valueOf(field);
        final Journal journal = getByIssn(issn);
        final List<Editor> editors = journal.getEditorialBoard().getEditors();

        Editor chosenEditor = null;
        for (Editor editor : editors) {
            if (editor.getScientificField().contains(scientificField)) {
                chosenEditor = editor;
                break;
            }
        }
        if (chosenEditor == null) {
            return false;
        }
        execution.setVariable("editor", chosenEditor.getUsername());
        emailService.sendMessage(chosenEditor.getUsername(),
                EmailService.SELECT_REVIEWERS_TITLE, EmailService.SELECT_REVIEWERS_MESSAGE);

        return true;
    }

    public void assignChiefEditor(String issn, DelegateExecution execution) {
        final Journal journal = getByIssn(issn);
        final Editor chiefEditor = journal.getEditorialBoard().getChiefEditor();
        execution.setVariable("editor", chiefEditor.getUsername());
        emailService.sendMessage(chiefEditor.getUsername(),
                EmailService.SELECT_REVIEWERS_TITLE, EmailService.SELECT_REVIEWERS_MESSAGE);
    }

    public String getCustomerId(String processInstanceId) {
        final String issn = (String) camundaService.getVariable("issn", processInstanceId);
        final Journal journal = getByIssn(issn);
        return journal.getPaymentId();
    }
}
