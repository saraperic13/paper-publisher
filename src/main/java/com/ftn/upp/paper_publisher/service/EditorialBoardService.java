package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.EditorialBoard;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.EditorialBoardDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditorialBoardService {

    private final EditorialBoardDao editorialBoardDao;

    @Autowired
    public EditorialBoardService(EditorialBoardDao editorialBoardDao) {
        this.editorialBoardDao = editorialBoardDao;
    }

    public EditorialBoard findByChiefEditor(Long chiefId) {
        return editorialBoardDao.getByChiefEditorId(chiefId).orElseThrow(
                () -> new EntityDoesNotExistException("Editorial board with given chief not found!"));
    }
}
