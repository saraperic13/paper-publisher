package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.controller.dto.UppTaskDto;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UppTaskService {

    private final CamundaService camundaService;

    @Autowired
    public UppTaskService(CamundaService camundaService) {
        this.camundaService = camundaService;
    }

    public List<UppTaskDto> getAllTasksByAssignee(String assignee) {
        final org.camunda.bpm.engine.rest.dto.task.TaskDto[] camundaTasks =
                camundaService.getAllTasksByAssignee(assignee);

        final List<UppTaskDto> uppTaskDtos = new ArrayList<>();

        for (TaskDto taskDto : camundaTasks) {
            final UppTaskDto task
                    = new UppTaskDto();
            task.setDue(taskDto.getDue());
            task.setName(taskDto.getName());
            task.setProcessInstanceId(taskDto.getProcessInstanceId());
            task.setTaskId(taskDto.getId());
            uppTaskDtos.add(task);
        }
        return uppTaskDtos;
    }
}