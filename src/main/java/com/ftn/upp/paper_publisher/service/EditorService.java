package com.ftn.upp.paper_publisher.service;

import com.ftn.upp.paper_publisher.domain.Editor;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.exception.EntityDoesNotExistException;
import com.ftn.upp.paper_publisher.repository.EditorDao;
import com.ftn.upp.paper_publisher.service.camunda.CamundaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class EditorService {

    private final CamundaService camundaService;

    private final EditorDao editorDao;

    @Autowired
    public EditorService(EditorDao editorDao, CamundaService camundaService) {
        this.editorDao = editorDao;
        this.camundaService = camundaService;
    }

    public Editor findByUsername(String username) {
        return editorDao.getByUsername(username).orElseThrow(()
                -> new EntityDoesNotExistException("Editor with given username does not exist!"));
    }

    public UserForm getForm(String processInstanceId) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        return new UserForm(processInstanceId, camundaService.getFormInHtml(taskId));
    }

    public void submitForm(String processInstanceId, HashMap<String, Object> map) {
        final String taskId = camundaService.getActiveTaskIdByProcessInstanceId(processInstanceId);
        camundaService.submitTaskForm(taskId, map);
    }
}
