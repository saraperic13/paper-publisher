package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.converter.ConvertDtoToMap;
import com.ftn.upp.paper_publisher.controller.dto.FormSubmissionDto;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RequestMapping("/api")
@Controller
public class RegistrationController {

    private RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping(path = "/registration_form",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getRegistrationForm() {

        final UserForm userForm = registrationService.getRegistrationForm();
        return new ResponseEntity<>(userForm, HttpStatus.OK);
    }

    @PostMapping(path = "/register/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity register(@RequestBody List<FormSubmissionDto> dto, @PathVariable String processInstanceId) {

        final HashMap<String, Object> map = ConvertDtoToMap.convert(dto);
        registrationService.register(processInstanceId, map);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
