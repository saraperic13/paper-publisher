package com.ftn.upp.paper_publisher.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReviewAuthorDto {

    private Long id;

    private Long paperId;

    private String comments;

    private Date submitted;
}
