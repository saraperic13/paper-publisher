package com.ftn.upp.paper_publisher.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScientificPaperDto {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private List<String> keywords;

    @NotNull
    private String paperAbstract;

    @NotNull
    private AccountDto author;

    private List<CoauthorDto> coauthors = new ArrayList<>();

    private List<ScientificPaperVersionDto> versions = new ArrayList<>();
}
