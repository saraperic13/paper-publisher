package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.converter.ConvertDtoToMap;
import com.ftn.upp.paper_publisher.controller.converter.ScientificPaperConverter;
import com.ftn.upp.paper_publisher.controller.dto.FormSubmissionDto;
import com.ftn.upp.paper_publisher.controller.dto.ScientificPaperDto;
import com.ftn.upp.paper_publisher.controller.dto.UploadedFileNameDto;
import com.ftn.upp.paper_publisher.domain.ScientificPaper;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.service.EmailService;
import com.ftn.upp.paper_publisher.service.ScientificPaperService;
import com.ftn.upp.paper_publisher.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/api/journal/")
@Controller
public class ScientificPaperController {
    
    private final ScientificPaperService scientificPaperService;

    private final StorageService storageService;

    @Autowired
    public ScientificPaperController(ScientificPaperService scientificPaperService,
                                     StorageService storageService) {
        this.scientificPaperService = scientificPaperService;
        this.storageService = storageService;
    }

    @GetMapping(value = "/papers/submitted_by/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getByProcessId(@PathVariable String processInstanceId) {
        final ScientificPaper paper = scientificPaperService.getByProcessInstanceId(processInstanceId);
        final ScientificPaperDto papersDto = ScientificPaperConverter.toDto(paper);
        return new ResponseEntity<>(papersDto, HttpStatus.OK);
    }

    @GetMapping(value = "/{journalId}/papers",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllPapersOfJournal(@PathVariable Long journalId) {
        final List<ScientificPaper> papers = scientificPaperService.getAll(journalId);
        final List<ScientificPaperDto> papersDto = papers.stream().map(
                ScientificPaperConverter::toDto).collect(Collectors.toList());

        return new ResponseEntity<>(papersDto, HttpStatus.OK);
    }

    @GetMapping(path = "/submit_paper_form/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getSubmitPaperForm(@PathVariable String processInstanceId) {

        final UserForm userForm = scientificPaperService.getForm(processInstanceId);
        return new ResponseEntity<>(userForm, HttpStatus.OK);
    }

    @PostMapping(value = "/upload",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity uploadPaper(@RequestParam("file") MultipartFile file) {

        final String fileName = storageService.store(file);
        return new ResponseEntity<>(new UploadedFileNameDto(fileName), HttpStatus.OK);
    }

    @PostMapping("/submit/{processInstanceId}")
    public ResponseEntity submitPaper(@PathVariable String processInstanceId,
                                      @RequestBody List<FormSubmissionDto> dto,
                                      @RequestParam String fileName) {


        final HashMap<String, Object> map = ConvertDtoToMap.convert(dto);
        scientificPaperService.submitFormAndSavePaper(processInstanceId, map, storageService.getFullPath(fileName));

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/reformat/{processInstanceId}")
    public ResponseEntity reformatPaper(@PathVariable String processInstanceId,
                                        @RequestParam String fileName) {


        scientificPaperService.submitReformattedPaper(processInstanceId, storageService.getFullPath(fileName));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/submit_revision/{processInstanceId}")
    public ResponseEntity submitRevision(@PathVariable String processInstanceId,
                                         @RequestBody List<FormSubmissionDto> dto,
                                         @RequestParam String fileName) {

        final HashMap<String, Object> map = ConvertDtoToMap.convert(dto);
        scientificPaperService.submitRevision(processInstanceId, storageService.getFullPath(fileName), map);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(
            value = "/download/{paperId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE

    )
    public ResponseEntity downloadPDF(@PathVariable Long paperId) {
        return ResponseEntity
                .ok()
                .header("Content-Disposition", "attachment; filename=paper.pdf")
                .body(scientificPaperService.getPaperPdf(paperId));
    }

}

