package com.ftn.upp.paper_publisher.controller.converter;

import com.ftn.upp.paper_publisher.controller.dto.FormSubmissionDto;

import java.util.HashMap;
import java.util.List;

public class ConvertDtoToMap {


    public static HashMap<String, Object> convert(List<FormSubmissionDto> list) {
        final HashMap<String, Object> map = new HashMap<>();

        String value;
        for (FormSubmissionDto temp : list) {
            value = temp.getFieldValue();
            if (value != null) {
                value = value.replaceAll("\\s+", "_");
                map.put(temp.getFieldId(), value);
            }

        }

        return map;
    }
}
