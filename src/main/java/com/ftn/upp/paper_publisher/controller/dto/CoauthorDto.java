package com.ftn.upp.paper_publisher.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CoauthorDto {

    private String name;

    private String email;

    private String address;
}
