package com.ftn.upp.paper_publisher.controller.converter;

import com.ftn.upp.paper_publisher.controller.dto.*;
import com.ftn.upp.paper_publisher.domain.*;

import java.util.stream.Collectors;

public class ScientificPaperConverter {

    public static ScientificPaperDto toDto(ScientificPaper scientificPaper) {

        final ScientificPaperDto scientificPaperDto = new ScientificPaperDto();
        scientificPaperDto.setId(scientificPaper.getId());
        scientificPaperDto.setName(scientificPaper.getName());
        scientificPaperDto.setPaperAbstract(scientificPaper.getPaperAbstract());
        scientificPaperDto.setKeywords(scientificPaper.getKeywords());


        scientificPaperDto.setAuthor(toAccountDto(scientificPaper.getAuthor()));

        scientificPaperDto.setCoauthors(
                scientificPaper.getCoauthors().stream().map(
                        ScientificPaperConverter::toCoauthorDtoFromCoauthor).collect(Collectors.toList()));

//        scientificPaperDto.setVersions(
//                scientificPaper.getVersions().stream().map(
//                        ScientificPaperConverter::toVersionDto).collect(Collectors.toList()));

        return scientificPaperDto;
    }

    private static AccountDto toAccountDto(Author author) {

        final AccountDto accountDto = new AccountDto();
        accountDto.setEmail(author.getUsername());
        accountDto.setFirstName(author.getFirstName());
        accountDto.setLastName(author.getLastName());
        if (author.getAddress() != null) {
            accountDto.setAddressDto(toAddressDto(author.getAddress()));
        }
        return accountDto;
    }


    private static AddressDto toAddressDto(Address address) {
        final AddressDto addressDto = new AddressDto();
        addressDto.setCity(address.getCity());
        addressDto.setCountry(address.getCountry());
        return addressDto;
    }


    private static CoauthorDto toCoauthorDtoFromCoauthor(Coauthor coauthor) {

        final CoauthorDto coauthorDto = new CoauthorDto();

        try {
            coauthorDto.setEmail(coauthor.getEmail());
            coauthorDto.setName(coauthor.getName());
            coauthor.setAddress(coauthor.getAddress());
        } catch (Exception e) {
        }
        return coauthorDto;
    }

    private static ScientificPaperVersionDto toVersionDto(ScientificPaperVersion version) {
        final ScientificPaperVersionDto dto = new ScientificPaperVersionDto();
        dto.setFinalVersion(version.isFinalVersion());
        dto.setId(version.getId());
        return dto;
    }
}
