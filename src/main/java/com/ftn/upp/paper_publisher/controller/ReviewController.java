package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.converter.ConvertDtoToMap;
import com.ftn.upp.paper_publisher.controller.converter.ReviewConverter;
import com.ftn.upp.paper_publisher.controller.dto.FormSubmissionDto;
import com.ftn.upp.paper_publisher.controller.dto.ReviewAuthorDto;
import com.ftn.upp.paper_publisher.controller.dto.ReviewEditorialBoardDto;
import com.ftn.upp.paper_publisher.domain.Review;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/reviews/")
public class ReviewController {

    private final ReviewService reviewService;

    @Autowired
    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping(value = "/form/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getFilterForm(@PathVariable String processInstanceId) {
        final UserForm userForm = reviewService.getForm(processInstanceId);
        return new ResponseEntity<>(userForm, HttpStatus.OK);
    }

    @PostMapping(value = "/submit/{processInstanceId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity submitFilterForm(@PathVariable String processInstanceId,
                                           @RequestBody List<FormSubmissionDto> dto) {
        final HashMap<String, Object> map = ConvertDtoToMap.convert(dto);
        reviewService.submitForm(processInstanceId, map);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/author/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllReviewsPaperForAuthor(@PathVariable String processInstanceId) {
        final List<Review> reviews = reviewService.getAllByProcessInstanceId(processInstanceId);
        final List<ReviewAuthorDto> dtos =
                reviews.stream().map(ReviewConverter::toAuthorDto).collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/editor/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllReviewsPaperForEditor(@PathVariable String processInstanceId) {
        final List<Review> reviews = reviewService.getAllByProcessInstanceId(processInstanceId);
        final List<ReviewEditorialBoardDto> dtos =
                reviews.stream().map(ReviewConverter::toEditorialDto).collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

}
