package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.converter.ReviewerConverter;
import com.ftn.upp.paper_publisher.controller.dto.ReviewerDto;
import com.ftn.upp.paper_publisher.controller.dto.UsernameDto;
import com.ftn.upp.paper_publisher.domain.Reviewer;
import com.ftn.upp.paper_publisher.service.ReviewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/reviewers/")
public class ReviewerController {

    private final ReviewerService reviewerService;

    @Autowired
    public ReviewerController(ReviewerService reviewerService) {
        this.reviewerService = reviewerService;
    }

    @GetMapping(value = "/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllReviewersFromJournal() {
        final List<Reviewer> reviewers = reviewerService.getAllFromJournal();
        final List<ReviewerDto> dtos = reviewers.stream().map(ReviewerConverter::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/filter/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity filter(@PathVariable String processInstanceId) {
        final List<Reviewer> reviewers = reviewerService.getReviewersFromScientificField(processInstanceId);
        final List<ReviewerDto> dtos = reviewers.stream().map(ReviewerConverter::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/select_form/submit/{processInstanceId}",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity submitSelectReviewersForm(@PathVariable String processInstanceId,
                                                    @RequestBody List<String> reviewers) {
        reviewerService.submitSelectReviewersForm(processInstanceId, reviewers);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/unassigned_reviewers/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUnassignedReviewers(@PathVariable String processInstanceId) {
        final List<Reviewer> reviewers = reviewerService.getUnassignedReviewers(processInstanceId);
        final List<ReviewerDto> dtos = reviewers.stream().map(ReviewerConverter::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/additional_reviewer/{processInstanceId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity assignAdditionalReviewer(@PathVariable String processInstanceId,
                                                   @RequestParam String reviewerUsername) {
        reviewerService.assignAdditionalReviewer(processInstanceId, reviewerUsername);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
