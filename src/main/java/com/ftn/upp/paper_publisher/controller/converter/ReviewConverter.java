package com.ftn.upp.paper_publisher.controller.converter;

import com.ftn.upp.paper_publisher.controller.dto.ReviewAuthorDto;
import com.ftn.upp.paper_publisher.controller.dto.ReviewEditorialBoardDto;
import com.ftn.upp.paper_publisher.domain.Review;

public class ReviewConverter {

    public static ReviewAuthorDto toAuthorDto(Review review) {

        final ReviewAuthorDto dto = new ReviewAuthorDto();
        dto.setComments(review.getComments());
        dto.setPaperId(review.getScientificPaper().getId());
        dto.setId(review.getId());
        dto.setSubmitted(review.getCreatedAt());

        return dto;
    }

    public static ReviewEditorialBoardDto toEditorialDto(Review review) {

        final ReviewEditorialBoardDto dto = new ReviewEditorialBoardDto();
        dto.setComments(review.getComments());
        dto.setCommentsForEditors(review.getCommentsForEditors());
        dto.setAdvice(review.getReviewAdviceType().toString());
        dto.setPaperId(review.getScientificPaper().getId());
        dto.setId(review.getId());
        dto.setReviewerFirstName(review.getReviewer().getFirstName());
        dto.setReviewerLastName(review.getReviewer().getLastName());
        dto.setSubmitted(review.getCreatedAt());

        return dto;
    }


}
