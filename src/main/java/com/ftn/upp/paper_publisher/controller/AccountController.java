package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.dto.LoginDto;
import com.ftn.upp.paper_publisher.controller.dto.TokenDto;
import com.ftn.upp.paper_publisher.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/account")
@CrossOrigin
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/login",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity login(@RequestBody @Valid LoginDto loginDto) {

        final String token = accountService.login(loginDto.getUsername(), loginDto.getPassword());
        return new ResponseEntity<>(new TokenDto(token), HttpStatus.OK);
    }

}