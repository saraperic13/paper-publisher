package com.ftn.upp.paper_publisher.controller.converter;

import com.ftn.upp.paper_publisher.controller.dto.ReviewerDto;
import com.ftn.upp.paper_publisher.domain.Reviewer;

public class ReviewerConverter {

    public static ReviewerDto toDto(Reviewer reviewer) {

        final ReviewerDto reviewerDto = new ReviewerDto();
        reviewerDto.setId(reviewer.getId());
        reviewerDto.setScientificFields(reviewer.getScientificFields());
        reviewerDto.setEmail(reviewer.getUsername());
        reviewerDto.setFirstName(reviewer.getFirstName());
        reviewerDto.setLastName(reviewer.getLastName());
        return reviewerDto;
    }
}
