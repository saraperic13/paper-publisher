package com.ftn.upp.paper_publisher.controller.converter;

import com.ftn.upp.paper_publisher.controller.dto.JournalDto;
import com.ftn.upp.paper_publisher.domain.Journal;

public class JournalConverter {

    public static JournalDto toDto(Journal journal) {
        final JournalDto dto = new JournalDto();
        dto.setId(journal.getId());
        dto.setIssn(journal.getIssn());
        dto.setName(journal.getName());
        return dto;
    }
}
