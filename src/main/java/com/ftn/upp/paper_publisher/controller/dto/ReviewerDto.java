package com.ftn.upp.paper_publisher.controller.dto;

import com.ftn.upp.paper_publisher.domain.enumeration.ScientificField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReviewerDto {

    @NotNull
    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String email;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = ScientificField.class)
    private List<ScientificField> scientificFields;
}
