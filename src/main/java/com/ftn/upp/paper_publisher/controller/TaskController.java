package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.service.UppTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/tasks")
public class TaskController {


    private final UppTaskService uppTaskService;

    @Autowired
    public TaskController(UppTaskService uppTaskService) {
        this.uppTaskService = uppTaskService;
    }

    @GetMapping(value = "/{assignee}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllByAssignee(@PathVariable String assignee) {
        return new ResponseEntity<>(uppTaskService.getAllTasksByAssignee(assignee), HttpStatus.OK);
    }
}
