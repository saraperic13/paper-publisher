package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.converter.ConvertDtoToMap;
import com.ftn.upp.paper_publisher.controller.dto.FormSubmissionDto;
import com.ftn.upp.paper_publisher.service.MembershipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/api/membership")
public class MembershipController {

    private final MembershipService membershipService;

    @Autowired
    public MembershipController(MembershipService membershipService) {
        this.membershipService = membershipService;
    }

    @PostMapping(value = "/proceed_payment/{processInstanceId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity submitForm(@PathVariable String processInstanceId,
                                               @RequestBody List<FormSubmissionDto> dto) {
        final HashMap<String, Object> map = ConvertDtoToMap.convert(dto);
        membershipService.submitForm(processInstanceId, map);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
