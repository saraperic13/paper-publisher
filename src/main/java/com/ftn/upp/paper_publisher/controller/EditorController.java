package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.converter.ConvertDtoToMap;
import com.ftn.upp.paper_publisher.controller.dto.FormSubmissionDto;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.service.EditorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/api/editor")
public class EditorController {

    private final EditorService editorService;

    @Autowired
    public EditorController(EditorService editorService) {
        this.editorService = editorService;
    }

    @GetMapping(value = "/form/{processInstanceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getPaperInfoForm(@PathVariable String processInstanceId) {
        final UserForm userForm = editorService.getForm(processInstanceId);
        return new ResponseEntity<>(userForm, HttpStatus.OK);
    }

    @PostMapping(value = "/form/{processInstanceId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity submitPaperInfoForm(@PathVariable String processInstanceId,
                                              @RequestBody List<FormSubmissionDto> dto) {
        final HashMap<String, Object> map = ConvertDtoToMap.convert(dto);
        editorService.submitForm(processInstanceId, map);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
