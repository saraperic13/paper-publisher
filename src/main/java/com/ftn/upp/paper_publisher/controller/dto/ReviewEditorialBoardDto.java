package com.ftn.upp.paper_publisher.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReviewEditorialBoardDto {

    private Long id;

    private String reviewerFirstName;

    private String reviewerLastName;

    private Long paperId;

    private String comments;

    private String advice;

    private String commentsForEditors;

    private Date submitted;

}
