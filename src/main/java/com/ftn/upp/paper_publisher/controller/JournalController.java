package com.ftn.upp.paper_publisher.controller;

import com.ftn.upp.paper_publisher.controller.converter.ConvertDtoToMap;
import com.ftn.upp.paper_publisher.controller.converter.JournalConverter;
import com.ftn.upp.paper_publisher.controller.dto.CustomerIdDto;
import com.ftn.upp.paper_publisher.controller.dto.FormSubmissionDto;
import com.ftn.upp.paper_publisher.controller.dto.JournalDto;
import com.ftn.upp.paper_publisher.domain.Journal;
import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import com.ftn.upp.paper_publisher.service.JournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/journals")
public class JournalController {

    private final JournalService journalService;

    @Autowired
    public JournalController(JournalService journalService) {
        this.journalService = journalService;
    }

    @GetMapping(value = "/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll() {

        final List<Journal> journals = journalService.getAll();
        final List<JournalDto> journalDtos = journals.stream().map(
                JournalConverter::toDto).collect(Collectors.toList());

        return new ResponseEntity<>(journalDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/payment_id/{processInstanceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getPaymentHubCustomerId(@PathVariable String processInstanceId) {

        final String id = journalService.getCustomerId(processInstanceId);
        return new ResponseEntity<>(new CustomerIdDto(id), HttpStatus.OK);
    }

    @GetMapping(value = "/journal_form", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity startSubmissionProcess() {
        final UserForm userForm = journalService.getChooseJournalForm();
        return new ResponseEntity<>(userForm, HttpStatus.OK);
    }

    @PostMapping(value = "/choose_journal/{processInstanceId}/{issn}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity chooseJournal(@PathVariable String processInstanceId,
                                        @PathVariable String issn) {
        journalService.submitChooseJournalForm(processInstanceId, issn);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
