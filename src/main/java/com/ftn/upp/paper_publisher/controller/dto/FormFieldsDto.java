package com.ftn.upp.paper_publisher.controller.dto;

import com.ftn.upp.paper_publisher.domain.camunda.UserForm;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.camunda.bpm.engine.form.FormField;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FormFieldsDto {

    private String processInstanceId;

    private List<FormField> formFields;

    public FormFieldsDto(UserForm userForm){
        this.processInstanceId = userForm.getProcessInstanceId();
//        this.formFields = userForm.getFormFields();

    }

}


