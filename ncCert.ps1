cd src/main/resources
C:\OpenSSL-Win32\bin\openssl genrsa -out nc.key 4096
C:\OpenSSL-Win32\bin\openssl req -x509 -new -nodes -key nc.key -out nccert.pem -sha256 -days 365 -passout 'pass:ncpass' -subj "/C=RS/ST=./L=Novi Sad/O=skmdns/OU=skmd/CN=*"
C:\OpenSSL-Win32\bin\openssl pkcs12 -export -out nckeystore.p12 -inkey nc.key -in nccert.pem -passout 'pass:ncpass'
