export enum RoleType {
  AUTHOR = "AUTHOR",
  REVIEWER = "REVIEWER",
  CHIEF_EDITOR = "CHIEF_EDITOR",
  EDITOR = "EDITOR",
}
