export class Address {
  id: number;
  country: string;
  city: string;
}
