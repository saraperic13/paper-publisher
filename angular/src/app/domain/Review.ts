export class Review {
  id: number;
  reviewerFirstName: string;
  reviewerLastName: string;
  paperId: number;
  comments: string;
  advice: string;
  commentsForEditors: string;

}
