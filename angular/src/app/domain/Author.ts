import {Address} from "./Address";

export class Author {
  id: number;
  firstName: string;
  lastName: string;
  address:Address;
}
