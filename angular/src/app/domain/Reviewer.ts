export class Reviewer {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  scientificFields: string[];
}
