import {Author} from "./Author";

export class Paper {
  id: number;
  name: string;
  author: Author;
  coauthors: Author[];
  paperAbstract: string;
  keywords: string[];
}
