export class Journal {
  id: number;
  name: string;
  issn: number;
}
