export class Task {
  taskId: string;
  name: string;
  processInstanceId: string;
  due: string;
}
