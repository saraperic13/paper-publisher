import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../errors/bad-request-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ConflictError} from "../../errors/conflict-error";
import {AppError} from "../../errors/app-error";
import {Review} from "../../domain/Review";

@Injectable()
export class ReviewService {

  private readonly basePath = 'api/reviews/';

  constructor(private http: HttpClient) {
  }

  getFrom(processInstanceId: string): Observable<any> {
    return this.http.get(`${this.basePath}form/${processInstanceId}`).catch(this.handleErrors);
  }

  submitForm(processInstanceId: string, dto: any): Observable<any> {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(`${this.basePath}submit/${processInstanceId}`, dto, {headers}).catch(this.handleErrors);
  }


  getReviewsByProcessId(processInstanceId: string):Observable<Review[]> {
    return this.http.get(`${this.basePath}editor/${processInstanceId}`).catch(this.handleErrors);
  }

  getReviewsByProcessIdForAuthor(processInstanceId: string):Observable<Review[]> {
    return this.http.get(`${this.basePath}author/${processInstanceId}`).catch(this.handleErrors);
  }

  protected handleErrors(response: HttpErrorResponse) {
    if (response.status === 400)
      return Observable.throw(new BadRequestError(response.error));
    else if (response.status === 403)
      return Observable.throw(new ForbiddenError(response.error));
    else if (response.status === 404)
      return Observable.throw(new NotFoundError(response.error));
    else if (response.status === 409)
      return Observable.throw(new ConflictError(response.error));
    return Observable.throw(new AppError(response.error));
  }

}
