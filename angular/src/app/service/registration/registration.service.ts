import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../errors/bad-request-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ConflictError} from "../../errors/conflict-error";
import {AppError} from "../../errors/app-error";

@Injectable()
export class RegistrationService {

  private readonly basePath = 'api/';

  constructor(private http: HttpClient) {
  }

  getRegistrationForm() {
    return this.http.get(this.basePath + "/registration_form").map((res: any) => {
      return res;
    }).catch(this.handleErrors);
  }


  register(registrationDto, processInstanceId: string) {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(this.basePath + "register/" + processInstanceId,
      registrationDto, {headers}).map((res: any) => {
      console.log(res);
      return res;
    }).catch(this.handleErrors);
  }

  protected handleErrors(response: HttpErrorResponse) {
    if (response.status === 400)
      return Observable.throw(new BadRequestError(response.error));
    else if (response.status === 403)
      return Observable.throw(new ForbiddenError(response.error));
    else if (response.status === 404)
      return Observable.throw(new NotFoundError(response.error));
    else if (response.status === 409)
      return Observable.throw(new ConflictError(response.error));
    return Observable.throw(new AppError(response.error));
  }

}
