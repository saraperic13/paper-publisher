import {Injectable} from '@angular/core';

@Injectable()
export class UtilsService {

  constructor() {
  }

  prepareDto() {
    let dto = [];
    let inputs = document.getElementsByTagName("input");
    let selects = document.getElementsByTagName("select");

    for (let i = 0; i < selects.length; i++) {
      let field = {};
      field["fieldId"] = selects[i].getAttribute("cam-variable-name");
      if (field["fieldId"] != null) {
        field["fieldValue"] = selects[i].options[selects[i].selectedIndex].value;
        dto.push(field);
      }
    }
    for (let i = 0; i < inputs.length; i++) {
      let field = {};
      field["fieldId"] = inputs[i].getAttribute("cam-variable-name");

      let type = inputs[i].getAttribute("type");
      console.log(type);

      if (field["fieldId"] == null) continue;

      field["fieldValue"] = inputs[i].value;
      if (type == "checkbox") {

        if (inputs[i].checked) {
          field["fieldValue"] = "true";
        } else {
          field["fieldValue"] = "false";
        }
      }

      dto.push(field);
    }
    return dto;
  }

  getCheckedBoxesValue(classname: string) {
    let dto = [];
    let inputs = document.getElementsByName(classname);
    for (let i = 0; i < inputs.length; i++) {
      let value = (<HTMLInputElement>inputs[i]).checked;
      if (value) {
        // let field = {};
        // field["username"] = (<HTMLInputElement>inputs[i]).value;
        dto.push((<HTMLInputElement>inputs[i]).value);
      }
    }
    return dto;
  }
}
