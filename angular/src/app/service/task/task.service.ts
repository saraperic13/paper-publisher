import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Task} from "../../domain/Task";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class TaskService {

  private readonly basePath = 'api/tasks/';

  constructor(private http: HttpClient) {
  }

  getMyTasks(): Observable<Task[]> {
    let username = localStorage.getItem('username');
    return this.http.get(this.basePath + username).catch((error: any) => {
      return Observable.throw(error || 'Server error');
    });
  }

}
