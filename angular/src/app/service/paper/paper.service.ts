import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import 'rxjs/Rx';
import {Paper} from "../../domain/Paper";
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../errors/bad-request-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ConflictError} from "../../errors/conflict-error";
import {AppError} from "../../errors/app-error";

@Injectable()
export class PaperService {

  private readonly basePath = 'api/journal/';

  constructor(private http: HttpClient) {
  }

  getAllPapersFromJournal(journalId: number): Observable<Paper[]> {
    return this.http.get(this.basePath + journalId + "/papers").map((res: any) => {
      return res;
    }).catch((error: any) => {
      return Observable.throw(error || 'Server error');
    });
  }

  getByProcessId(processInstanceId): Observable<Paper> {
    return this.http.get(this.basePath + "papers/submitted_by/" + processInstanceId).catch((error: any) => {
      return Observable.throw(error || 'Server error');
    });
  }

  getSubmitPaperForm(): Observable<any> {
    let processInstanceId = localStorage.getItem("processInstanceId");
    return this.http.get(this.basePath + "/submit_paper_form/" + processInstanceId).map((res: any) => {
      return res;
    }).catch(this.handleErrors);
  }

  submit(submitDto, fileName): Observable<any> {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    let processInstanceId = localStorage.getItem("processInstanceId");

    return this.http.post(`${this.basePath}submit/${processInstanceId}?fileName=${fileName}`, submitDto, {headers})
      .catch(this.handleErrors);
  }


  submit_reformatted(fileName): Observable<any> {
    let processInstanceId = localStorage.getItem("processInstanceId");

    return this.http.post(`${this.basePath}reformat/${processInstanceId}?fileName=${fileName}`, {})
      .catch(this.handleErrors);
  }


  submit_revision(submitDto, fileName): Observable<any> {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    let processInstanceId = localStorage.getItem("processInstanceId");

    return this.http.post(`${this.basePath}submit_revision/${processInstanceId}?fileName=${fileName}`,
      submitDto, {headers})
      .catch(this.handleErrors);
  }


  uploadPaper(formData): Observable<any> {
    return this.http.post(`${this.basePath}/upload/`, formData).catch(this.handleErrors);
  }

  downloadPdf(scientificPaperId: number): Observable<Blob> {
    let headers = new HttpHeaders({'Content-Type': 'application/pdf;charset=UTF-8', 'Accept': 'application/pdf'});

    return this.http.get(`${this.basePath}/download/${scientificPaperId}`, {
      headers,
      responseType: 'blob'
    }).map((response: any) => {

      return new Blob([response], {type: 'application/pdf'});

    }).catch((error: any) => {
      return Observable.throw(error || 'Server error');

    });

  }

  protected handleErrors(response: HttpErrorResponse) {
    if (response.status === 400)
      return Observable.throw(new BadRequestError(response.error));
    else if (response.status === 403)
      return Observable.throw(new ForbiddenError(response.error));
    else if (response.status === 404)
      return Observable.throw(new NotFoundError(response.error));
    else if (response.status === 409)
      return Observable.throw(new ConflictError(response.error));
    return Observable.throw(new AppError(response.error));
  }

}
