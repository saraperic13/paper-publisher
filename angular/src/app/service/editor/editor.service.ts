import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../errors/bad-request-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ConflictError} from "../../errors/conflict-error";
import {AppError} from "../../errors/app-error";

@Injectable()
export class EditorService {

  private readonly basePath = '/api/editor/';

  constructor(private http: HttpClient) {
  }

  getPaperInfoForm(processInstanceId: string): Observable<any> {
    return this.http.get(this.basePath + "form/" + processInstanceId).map((res: any) => {
      return res;
    }).catch(this.handleErrors);
  }

  submit(processInstanceId: string, submitDto): Observable<any> {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.post(`${this.basePath}form/${processInstanceId}`, submitDto, {headers})
      .catch(this.handleErrors);
  }

  protected handleErrors(response: HttpErrorResponse) {
    if (response.status === 400)
      return Observable.throw(new BadRequestError(response.error));
    else if (response.status === 403)
      return Observable.throw(new ForbiddenError(response.error));
    else if (response.status === 404)
      return Observable.throw(new NotFoundError(response.error));
    else if (response.status === 409)
      return Observable.throw(new ConflictError(response.error));
    return Observable.throw(new AppError(response.error));
  }
}
