import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Journal} from "../../domain/Journal";
import {BadRequestError} from "../../errors/bad-request-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ConflictError} from "../../errors/conflict-error";
import {AppError} from "../../errors/app-error";

@Injectable()
export class JournalService {

  private readonly basePath = 'api/journals/';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<Journal[]> {
    return this.http.get(this.basePath).map((res: any) => {
      return res;
    }).catch(this.handleErrors);
  }

  getPaymentHubCustomerId(processInstanceId: string): Observable<any> {
    return this.http.get(`${this.basePath}/payment_id/${processInstanceId}`).catch(this.handleErrors);
  }

  getChooseJournalForm(): Observable<any> {
    return this.http.get(this.basePath + "/journal_form").catch(this.handleErrors);
  }


  chooseJournal(processInstanceId: string, issn: string): Observable<any> {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(this.basePath + "choose_journal/" + processInstanceId + "/" + issn, {headers})
      .map((res: any) => {
        console.log(res);
        return res;
      }).catch(this.handleErrors);
  }

  protected handleErrors(response: HttpErrorResponse) {
    if (response.status === 400)
      return Observable.throw(new BadRequestError(response.error));
    else if (response.status === 403)
      return Observable.throw(new ForbiddenError(response.error));
    else if (response.status === 404)
      return Observable.throw(new NotFoundError(response.error));
    else if (response.status === 409)
      return Observable.throw(new ConflictError(response.error));
    return Observable.throw(new AppError(response.error));
  }


}
