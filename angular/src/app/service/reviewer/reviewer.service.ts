import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Reviewer} from "../../domain/Reviewer";
import {BadRequestError} from "../../errors/bad-request-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ConflictError} from "../../errors/conflict-error";
import {AppError} from "../../errors/app-error";

@Injectable()
export class ReviewerService {

  private readonly basePath = 'api/reviewers/';

  constructor(private http: HttpClient) {
  }

  getAllReviewersFromJournal(): Observable<Reviewer[]> {
    return this.http.get(this.basePath).catch(this.handleErrors);
  }

  getAllUnassignedReviewersFromJournal(processInstanceId: string): Observable<Reviewer[]> {
    return this.http.get(`${this.basePath}/unassigned_reviewers/${processInstanceId}`).catch(this.handleErrors);
  }

  filterReviewersFromJournal(processInstanceId: string): Observable<Reviewer[]> {
    return this.http.get(`${this.basePath}filter/${processInstanceId}`).catch(this.handleErrors);
  }

  submit(submitDto, processInstanceId: string): Observable<any> {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(`${this.basePath}select_form/submit/${processInstanceId}`, submitDto, {headers})
      .catch(this.handleErrors);
  }

  additionalReviewer(processInstanceId: string, username:string): Observable<any> {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(`${this.basePath}/additional_reviewer/${processInstanceId}?reviewerUsername=${username}`,
      {headers})
      .catch(this.handleErrors);
  }


  private handleErrors(response: HttpErrorResponse) {
    if (response.status === 400)
      return Observable.throw(new BadRequestError(response.error));
    else if (response.status === 403)
      return Observable.throw(new ForbiddenError(response.error));
    else if (response.status === 404)
      return Observable.throw(new NotFoundError(response.error));
    else if (response.status === 409)
      return Observable.throw(new ConflictError(response.error));
    return Observable.throw(new AppError(response.error));
  }
}
