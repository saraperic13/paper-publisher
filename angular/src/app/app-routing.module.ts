import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoginComponent} from "./component/login/login.component";
import {NotFoundPageComponent} from "./component/not-found-page/not-found-page.component";
import {JournalsComponent} from "./component/journals/journals.component";
import {SubmitPaperFormComponent} from "./component/submit-paper-form/submit-paper-form.component";
import {RegistrationPageComponent} from "./component/registration/registration-page/registration-page.component";
import {TasksPageComponent} from "./component/tasks-page/tasks-page.component";
import {ViewPaperInfoComponent} from "./component/view-paper-info/view-paper-info.component";
import {SelectReviewersComponent} from "./component/select-reviewers/select-reviewers.component";
import {ReviewFormComponent} from "./component/review-form/review-form.component";
import {AnalyzeReviewsComponent} from "./component/analyze-reviews/analyze-reviews.component";
import {SubmitRevisionComponent} from "./component/submit-revision/submit-revision.component";
import {PaymentComponent} from "./component/payment/payment.component";
import {PaymentSuccessComponent} from "./component/payment-success/payment-success.component";

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: '/login', pathMatch: 'full'
  },
  {
    path: 'journals',
    loadChildren: 'app/component/home/home.module#HomeModule'
  },
  {
    path: 'register',
    component: RegistrationPageComponent
  },
  {
    path: 'submit_paper/:processInstanceId',
    component: SubmitPaperFormComponent
  },
  {
    path: 'my_tasks',
    component: TasksPageComponent
  },
  {
    path: 'start_submission/:processInstanceId',
    component: JournalsComponent
  },
  {
    path: 'paper_info/:processInstanceId',
    component: ViewPaperInfoComponent
  },
  {
    path: 'select_reviewers/:processInstanceId',
    component: SelectReviewersComponent
  },
  {
    path: 'review_paper/:processInstanceId',
    component: ReviewFormComponent
  },
  {
    path: 'analyze_reviews/:processInstanceId',
    component: AnalyzeReviewsComponent
  },
  {
    path: 'submit_revision/:processInstanceId',
    component: SubmitRevisionComponent
  },
  {
    path: 'pay_fee/:processInstanceId',
    component: PaymentComponent
  },
  {
    path: 'return_url',
    component: PaymentSuccessComponent
  },
  {
    path: '**',
    component: NotFoundPageComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}


