import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavbarComponent} from "./component/navbar/navbar/navbar.component";
import {AccountService} from "./service/account/account.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CanActivateAuthGuard} from "./guard/can-activate-auth-guard";
import {TokenInterceptorService} from "./interceptor/TokenInterceptorService";
import {LoginComponent} from "./component/login/login.component";
import {NotFoundPageComponent} from "./component/not-found-page/not-found-page.component";
import {PaperService} from "./service/paper/paper.service";
import {JournalService} from "./service/journal/journal.service";
import {HomeModule} from "./component/home/home.module";
import {ToasterModule} from "angular5-toaster/dist";
import {SubmitPaperComponent} from './component/submit-paper/submit-paper.component';
import {JournalsComponent} from "./component/journals/journals.component";
import {SubmitPaperFormComponent} from './component/submit-paper-form/submit-paper-form.component';
import {SafeHtmlPipe} from "./pipes/safe-html.pipe";
import {RegistrationFormComponent} from "./component/registration/registration-form/registration-form.component";
import {RegistrationPageComponent} from "./component/registration/registration-page/registration-page.component";
import {RegistrationService} from "./service/registration/registration.service";
import {TasksPageComponent} from './component/tasks-page/tasks-page.component';
import {TaskComponent} from './component/task/task.component';
import {TaskService} from "./service/task/task.service";
import {UtilsService} from "./service/utils/utils.service";
import {ViewPaperInfoComponent} from './component/view-paper-info/view-paper-info.component';
import {EditorService} from "./service/editor/editor.service";
import {PaperCardComponent} from "./component/paper-card/paper-card.component";
import {SelectReviewersComponent} from './component/select-reviewers/select-reviewers.component';
import {ReviewerService} from "./service/reviewer/reviewer.service";
import {ReviewService} from "./service/review/review.service";
import {ReviewFormComponent} from './component/review-form/review-form.component';
import {ReviewCardComponent} from './component/review-card/review-card.component';
import {ReviewListComponent} from './component/review-list/review-list.component';
import {AnalyzeReviewsComponent} from './component/analyze-reviews/analyze-reviews.component';
import {SubmitRevisionComponent} from './component/submit-revision/submit-revision.component';
import {PaymentComponent} from './component/payment/payment.component';
import {MembershipService} from "./service/membership/membership.service";
import { PaymentSuccessComponent } from './component/payment-success/payment-success.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    NotFoundPageComponent,
    SubmitPaperComponent,
    JournalsComponent,
    SubmitPaperFormComponent,
    SafeHtmlPipe,
    RegistrationPageComponent,
    RegistrationFormComponent,
    TasksPageComponent,
    TaskComponent,
    ViewPaperInfoComponent,
    PaperCardComponent,
    SelectReviewersComponent,
    ReviewFormComponent,
    ReviewCardComponent,
    ReviewListComponent,
    AnalyzeReviewsComponent,
    SubmitRevisionComponent,
    PaymentComponent,
    PaymentSuccessComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HomeModule,
    ToasterModule
  ],
  providers: [
    AccountService,
    CanActivateAuthGuard,
    PaperService,
    JournalService,
    RegistrationService,
    TaskService,
    UtilsService,
    EditorService,
    ReviewerService,
    ReviewService,
    MembershipService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }],

  bootstrap: [AppComponent]
})
export class AppModule {
}
