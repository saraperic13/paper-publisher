import {Component, OnInit} from '@angular/core';
import {Review} from "../../domain/Review";
import {SafeHtmlPipe} from "../../pipes/safe-html.pipe";
import {ReviewService} from "../../service/review/review.service";
import {ToasterService} from "angular5-toaster/dist";
import {ActivatedRoute, Router} from "@angular/router";
import {UtilsService} from "../../service/utils/utils.service";
import {Paper} from "../../domain/Paper";
import {PaperService} from "../../service/paper/paper.service";
import {EditorService} from "../../service/editor/editor.service";

@Component({
  selector: 'app-analyze-reviews',
  templateUrl: './analyze-reviews.component.html',
  styleUrls: ['./analyze-reviews.component.css']
})
export class AnalyzeReviewsComponent implements OnInit {

  reviewsForPaper: Review[];

  html: SafeHtmlPipe;
  processInstanceId: string;
  paper: Paper;

  constructor(private reviewService: ReviewService,
              private paperService: PaperService,
              private editorService: EditorService,
              private toasterService: ToasterService,
              private utilsService: UtilsService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.processInstanceId = params["processInstanceId"];
      console.log(this.processInstanceId);
      localStorage.setItem("processInstanceId", this.processInstanceId);
      this.getPaper(this.processInstanceId);
      this.getAllReviews(this.processInstanceId);
      this.getForm();
    });
  }

  getPaper(processInstanceId: string) {
    this.paperService.getByProcessId(processInstanceId).subscribe(
      (res) => {
        this.paper = res;
      });
  }

  getAllReviews(processInstanceId: string) {
    console.log("POZIVAAAAAM");
    this.reviewService.getReviewsByProcessId(processInstanceId).subscribe(
      (res) => {
        console.log("REVIEWS");
        console.log(res);
        this.reviewsForPaper = res;
      });
  }

  getForm() {
    this.editorService.getPaperInfoForm(this.processInstanceId).subscribe((res) => {
      this.html = res.htmlForm;
    });
  }

  submit() {
    let dto = this.utilsService.prepareDto();
    this.editorService.submit(this.processInstanceId, dto).subscribe((res) => {
      this.navigate();
    });
  }

  navigate() {
    this.router.navigate(["/my_tasks"]);
  }

}
