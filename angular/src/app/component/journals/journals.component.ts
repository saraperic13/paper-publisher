import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {JournalService} from "../../service/journal/journal.service";
import {Journal} from "../../domain/Journal";
import {AppError} from "../../errors/app-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {BadRequestError} from "../../errors/bad-request-error";
import {ToasterService} from "angular5-toaster/dist";

@Component({
  selector: 'app-journals',
  templateUrl: './journals.component.html',
  styleUrls: ['./journals.component.css']
})
export class JournalsComponent implements OnInit {

  journals: Journal[];

  constructor(private journalService: JournalService,
              private router: Router,
              private toasterService: ToasterService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let processInstanceId = params["processInstanceId"];
      if (!processInstanceId || processInstanceId=="undefined") {
        console.log("nema procesa ")
        this.getForm();
      } else {
        console.log("ima procesa " + processInstanceId)
        localStorage.setItem("processInstanceId", processInstanceId);
        this.getJournals();
      }
    });
  }

  getJournals() {
    this.journalService.getAll().subscribe((res) => {
      this.journals = res;
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  getForm() {
    console.log("STARTED PROCESS");
    this.journalService.getChooseJournalForm().subscribe((res) => {
      console.log(res.processInstanceId);
      localStorage.setItem("processInstanceId", res.processInstanceId);
      this.getJournals();
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  chooseJournal(issn) {
    this.journalService.chooseJournal(localStorage.getItem("processInstanceId"), issn).subscribe((res) => {
      console.log(res);
      this.router.navigate(['/my_tasks']);
    })
  }

  handleError(error: AppError) {
    console.log(error);
    if (error instanceof NotFoundError)
      this.toasterService.pop('error', 'Error', error.message);
    else if (error instanceof ForbiddenError)
      this.toasterService.pop('error', 'Error', error.message);
    else if (error instanceof BadRequestError)
      this.toasterService.pop('error', 'Error', error.message);
    else {
      this.toasterService.pop('error', 'Error', error.message);
      throw error;
    }
  }
}
