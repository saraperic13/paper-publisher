import {Component, Input, OnInit} from '@angular/core';
import {Review} from "../../domain/Review";

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.css']
})
export class ReviewListComponent implements OnInit {

  @Input() reviews: Review[];

  constructor() {
  }

  ngOnInit() {
  }

}
