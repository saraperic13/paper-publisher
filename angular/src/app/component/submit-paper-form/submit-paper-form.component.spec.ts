import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitPaperFormComponent } from './submit-paper-form.component';

describe('SubmitPaperFormComponent', () => {
  let component: SubmitPaperFormComponent;
  let fixture: ComponentFixture<SubmitPaperFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitPaperFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitPaperFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
