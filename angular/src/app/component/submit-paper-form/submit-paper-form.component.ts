import {Component, OnInit} from '@angular/core';
import {SafeHtmlPipe} from "../../pipes/safe-html.pipe";
import {PaperService} from "../../service/paper/paper.service";
import {AppError} from "../../errors/app-error";
import {ToasterService} from "angular5-toaster/dist";
import {UtilsService} from "../../service/utils/utils.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-submit-paper-form',
  templateUrl: './submit-paper-form.component.html',
  styleUrls: ['./submit-paper-form.component.css']
})
export class SubmitPaperFormComponent implements OnInit {

  html: SafeHtmlPipe;
  fileToUpload: File = null;

  reformat: boolean = false;

  constructor(private paperService: PaperService,
              private toasterService: ToasterService, private utilsService: UtilsService,
              private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.reformat = params["reformat"];
      if (!this.reformat) {
        console.log("nema papera ")
        this.getSubmitPaperForm();
      } else {
        console.log("ima papera ")
      }
      console.log(this.reformat);
    });

  }

  getSubmitPaperForm() {
    this.paperService.getSubmitPaperForm().subscribe((res) => {
      console.log(res);
      this.html = res.htmlForm;
    }, (error: AppError) => {
      console.log(error);
    });

  }

  upload() {
    let body = new FormData();
    body.append("file", this.fileToUpload);

    this.paperService.uploadPaper(body).subscribe((uploadedFile) => {
      if (this.reformat) {
        this.submit_reformatted(uploadedFile.fileName);
      } else {
        this.submit(uploadedFile.fileName);
      }
    }, (error: AppError) => {
      console.log(error);
    });
  }

  handleFileInput(event: any) {
    this.fileToUpload = event.target.files[0];
  }


  submit_reformatted(fileName: string) {
    this.paperService.submit_reformatted(fileName).subscribe((res) => {

      this.toasterService.popAsync('success', 'Success', "Successful submit!");
      let self = this;
      setTimeout(function () {
        self.navigate();
      }, 1000);


    }, (error: AppError) => {
      console.log(error);
    });
  }

  submit(fileName: string) {
    let dto = this.utilsService.prepareDto();
    console.log(dto);
    this.paperService.submit(dto, fileName).subscribe((res) => {

      this.toasterService.popAsync('success', 'Success', "Successful submit!");
      let self = this;
      setTimeout(function () {
        self.navigate();
      }, 1000);


    }, (error: AppError) => {
      console.log(error);
    });
  }

  navigate() {
    this.router.navigate(["/my_tasks"]);
  }
}
