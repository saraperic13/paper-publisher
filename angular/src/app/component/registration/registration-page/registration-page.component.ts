import {Component, OnInit} from '@angular/core';
import {ToasterConfig} from "angular5-toaster/dist";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit {

  toasterConfig: ToasterConfig;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000}
    });
  }

  onRegister() {
    let self = this;
    setTimeout(function () {
      self.navigate();
    }, 1000);

  }

  navigate(){
    this.router.navigate(["/login"]);
  }

}
