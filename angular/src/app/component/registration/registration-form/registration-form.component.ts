import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RegistrationService} from "../../../service/registration/registration.service";
import {SafeHtmlPipe} from "../../../pipes/safe-html.pipe";
import {ToasterService} from "angular5-toaster/dist";
import {AppError} from "../../../errors/app-error";
import {NotFoundError} from "../../../errors/not-found-error";
import {ForbiddenError} from "../../../errors/forbidden-error";
import {BadRequestError} from "../../../errors/bad-request-error";


@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {

  @Output() registeredEvent = new EventEmitter();

  html: SafeHtmlPipe;
  processInstanceId: string;

  constructor(private registrationService: RegistrationService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.getRegistrationForm();
  }

  getRegistrationForm() {
    this.registrationService.getRegistrationForm().subscribe((res) => {
      console.log(res);
      this.processInstanceId = res.processInstanceId;
      // this.html = res.htmlForm;
      this.html = res.htmlForm;
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  register() {
    let dto = this.prepareRegisterDto();
    console.log(dto);
    this.registrationService.register(dto, this.processInstanceId).subscribe((res) => {

      this.toasterService.popAsync('success', 'Success', "You are registered!");
      this.registeredEvent.emit();

      }, (error: AppError) => {
      this.handleError(error);
    });
  }

  prepareRegisterDto() {
    let dto = [];
    let inputs = document.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
      let field = {};
      field["fieldId"] = inputs[i].getAttribute("cam-variable-name");
      field["fieldValue"] = inputs[i].value;
      dto.push(field);
    }
    return dto;
  }

  handleError(error: AppError) {
    console.log(error);
    if (error instanceof NotFoundError)
      this.toasterService.pop('error', 'Error', error.message);
    else if (error instanceof ForbiddenError)
      this.toasterService.pop('error', 'Error', error.message);
    else if (error instanceof BadRequestError)
      this.toasterService.pop('error', 'Error', error.message);
    else {
      this.toasterService.pop('error', 'Error', error.message);
      throw error;
    }
  }
}
