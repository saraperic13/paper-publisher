import {Component, OnInit} from '@angular/core';
import {SafeHtmlPipe} from "../../pipes/safe-html.pipe";
import {ReviewService} from "../../service/review/review.service";
import {ToasterService} from "angular5-toaster/dist";
import {ActivatedRoute, Router} from "@angular/router";
import {UtilsService} from "../../service/utils/utils.service";
import {Paper} from "../../domain/Paper";
import {PaperService} from "../../service/paper/paper.service";

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.css']
})
export class ReviewFormComponent implements OnInit {

  html: SafeHtmlPipe;
  processInstanceId: string;
  paper: Paper;

  constructor(private reviewService: ReviewService,
              private paperService: PaperService,
              private toasterService: ToasterService,
              private utilsService: UtilsService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.processInstanceId = params["processInstanceId"];
      console.log(this.processInstanceId);
      localStorage.setItem("processInstanceId", this.processInstanceId);
      this.getPaper(this.processInstanceId);
      this.getForm();
    });
  }

  getPaper(processInstanceId: string) {
    console.log(processInstanceId);
    this.paperService.getByProcessId(processInstanceId).subscribe(
      (res) => {
        this.paper = res;
      });
  }

  getForm() {
    this.reviewService.getFrom(this.processInstanceId).subscribe((res) => {
      this.html = res.htmlForm;
    });
  }

  submit() {
    let dto = this.utilsService.prepareDto();
    this.reviewService.submitForm(this.processInstanceId, dto).subscribe((res) => {

      this.toasterService.popAsync('success', 'Success', "Successful submit!");
      let self = this;
      setTimeout(function () {
        self.navigate();
      }, 1000);

    });
  }

  navigate() {
    this.router.navigate(["/my_tasks"]);
  }

}
