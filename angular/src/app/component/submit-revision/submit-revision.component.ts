import {Component, OnInit} from '@angular/core';
import {Review} from "../../domain/Review";
import {ActivatedRoute, Router} from "@angular/router";
import {UtilsService} from "../../service/utils/utils.service";
import {ToasterService} from "angular5-toaster/dist";
import {EditorService} from "../../service/editor/editor.service";
import {PaperService} from "../../service/paper/paper.service";
import {ReviewService} from "../../service/review/review.service";
import {SafeHtmlPipe} from "../../pipes/safe-html.pipe";
import {AppError} from "../../errors/app-error";

@Component({
  selector: 'app-submit-revision',
  templateUrl: './submit-revision.component.html',
  styleUrls: ['./submit-revision.component.css']
})
export class SubmitRevisionComponent implements OnInit {

  paperReviews: Review[];

  processInstanceId: string;
  html: SafeHtmlPipe;
  fileToUpload: File = null;


  constructor(private reviewService: ReviewService,
              private paperService: PaperService,
              private editorService: EditorService,
              private toasterService: ToasterService,
              private utilsService: UtilsService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.processInstanceId = params["processInstanceId"];
      console.log(this.processInstanceId);
      localStorage.setItem("processInstanceId", this.processInstanceId);
      this.getAllReviews(this.processInstanceId);
      this.getForm();
    });
  }


  getForm() {
    this.paperService.getSubmitPaperForm().subscribe((res) => {
      console.log(res);
      this.html = res.htmlForm;
    }, (error: AppError) => {
      console.log(error);
    });

  }

  getAllReviews(processInstanceId: string) {
    this.reviewService.getReviewsByProcessIdForAuthor(processInstanceId).subscribe(
      (res) => {
        this.paperReviews = res;
      });
  }

  upload() {
    let body = new FormData();
    body.append("file", this.fileToUpload);

    this.paperService.uploadPaper(body).subscribe((uploadedFile) => {
      this.submit(uploadedFile.fileName);

    }, (error: AppError) => {
      console.log(error);
    });
  }

  handleFileInput(event: any) {
    this.fileToUpload = event.target.files[0];
  }

  submit(fileName: string) {
    let dto = this.utilsService.prepareDto();
    console.log(dto);
    this.paperService.submit_revision(dto, fileName).subscribe((res) => {

      this.toasterService.popAsync('success', 'Success', "Successful submit!");
      let self = this;
      setTimeout(function () {
        self.navigate();
      }, 1000);
    }, (error: AppError) => {
      console.log(error);
    });
  }

  navigate() {
    this.router.navigate(["/my_tasks"]);
  }
}
