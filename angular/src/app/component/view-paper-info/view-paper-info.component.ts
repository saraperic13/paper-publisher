import {Component, OnInit} from '@angular/core';
import {Paper} from "../../domain/Paper";
import {SafeHtmlPipe} from "../../pipes/safe-html.pipe";
import {PaperService} from "../../service/paper/paper.service";
import {ToasterService} from "angular5-toaster/dist";
import {UtilsService} from "../../service/utils/utils.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AppError} from "../../errors/app-error";
import {EditorService} from "../../service/editor/editor.service";

@Component({
  selector: 'app-view-paper-info',
  templateUrl: './view-paper-info.component.html',
  styleUrls: ['./view-paper-info.component.css']
})
export class ViewPaperInfoComponent implements OnInit {

  paper: Paper;
  html: SafeHtmlPipe;

  constructor(private paperService: PaperService,
              private editorService: EditorService,
              private toasterService: ToasterService, private utilsService: UtilsService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let processInstanceId = params["processInstanceId"];
      console.log(processInstanceId);
      localStorage.setItem("processInstanceId", processInstanceId);
      this.getPaper(processInstanceId);
      this.getPaperInfoForm(processInstanceId);
    });
  }

  getPaper(processInstanceId: string) {
    console.log(processInstanceId);
    this.paperService.getByProcessId(processInstanceId).subscribe(
      (res) => {
        this.paper = res;
      });
  }

  getPaperInfoForm(processInstanceId: string) {
    this.editorService.getPaperInfoForm(processInstanceId).subscribe((res) => {
      console.log(res);
      this.html = res.htmlForm;
    }, (error: AppError) => {
      console.log(error);
    });
  }

  submit() {
    let dto = this.utilsService.prepareDto();
    console.log(dto);
    this.editorService.submit(localStorage.getItem("processInstanceId"), dto).subscribe((res) => {

      // this.getPaperInfoForm(localStorage.getItem("processInstanceId"));
      this.router.navigate(["/my_tasks"]);

    }, (error: AppError) => {
      console.log(error);
    });
  }

}
