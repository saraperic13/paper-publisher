import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPaperInfoComponent } from './view-paper-info.component';

describe('ViewPaperInfoComponent', () => {
  let component: ViewPaperInfoComponent;
  let fixture: ComponentFixture<ViewPaperInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPaperInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPaperInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
