import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from "../../domain/Task";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input() task: Task;
  @Output() onTaskClicked = new EventEmitter<Task>();

  constructor() { }

  ngOnInit() {
  }

  goToTask(task){
    console.log(task);
    this.onTaskClicked.emit(task);
  }


}
