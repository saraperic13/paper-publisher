import {Component, OnInit} from '@angular/core';
import {ToasterService} from "angular5-toaster/dist";
import {UtilsService} from "../../service/utils/utils.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ReviewerService} from "../../service/reviewer/reviewer.service";
import {Reviewer} from "../../domain/Reviewer";

@Component({
  selector: 'app-select-reviewers',
  templateUrl: './select-reviewers.component.html',
  styleUrls: ['./select-reviewers.component.css']
})
export class SelectReviewersComponent implements OnInit {

  reviewers: Reviewer[];
  processInstanceId: string;
  isChecked: boolean;
  onlyAdditionalReviewer: boolean = false;

  constructor(private reviewerService: ReviewerService,
              private toasterService: ToasterService, private utilsService: UtilsService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let processInstanceId = params["processInstanceId"];
      localStorage.setItem("processInstanceId", processInstanceId);
      this.processInstanceId = processInstanceId;

      this.route.queryParams.subscribe(queryParams => {
        let unassigned = queryParams["unassigned"];
        console.log(unassigned == "true");
        if (unassigned == "true") {
          this.getAdditionalReviewer();
          this.onlyAdditionalReviewer = true;
        }
        else {
          this.getAllReviewers();
        }
      });
    });
  }

  getAllReviewers() {
    this.reviewerService.getAllReviewersFromJournal().subscribe((res) => {
      this.reviewers = res;
    });
  }

  getAdditionalReviewer() {
    console.log("samo additional");
    this.reviewerService.getAllUnassignedReviewersFromJournal(this.processInstanceId).subscribe((res) => {
      this.reviewers = res;
    });
  }

  get(event: string) {
    console.log(event);
    if (event == "filtered") {
      this.filter();
    } else {
      if (this.onlyAdditionalReviewer) {
        this.getAdditionalReviewer();
      } else {
        this.getAllReviewers();
      }
    }
  }

  filter() {
    this.reviewerService.filterReviewersFromJournal(this.processInstanceId).subscribe(res => {
      this.reviewers = res;
    })
  }

  submit() {
    if (this.onlyAdditionalReviewer) {
      this.selectAdditionalReviewer();
    } else {
      this.selectReviewers();
    }
  }

  selectReviewers() {
    let reviewersDto = this.utilsService.getCheckedBoxesValue("reviewer-checkbox");
    console.log(reviewersDto);
    this.reviewerService.submit(reviewersDto, this.processInstanceId).subscribe((res) => {
      console.log(res);
      this.router.navigate(["/my_tasks"]);
    });
  }

  selectAdditionalReviewer() {
    let reviewersDto = this.utilsService.getCheckedBoxesValue("reviewer-checkbox");
    console.log(reviewersDto);
    this.reviewerService.additionalReviewer(this.processInstanceId, reviewersDto[0]).subscribe((res) => {
      console.log(res);
      this.router.navigate(["/my_tasks"]);
    });
  }

}
