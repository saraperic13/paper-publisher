import {Component, OnInit} from '@angular/core';
import {Task} from "../../domain/Task";
import {TaskService} from "../../service/task/task.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tasks-page',
  templateUrl: './tasks-page.component.html',
  styleUrls: ['./tasks-page.component.css']
})
export class TasksPageComponent implements OnInit {

  tasks: Task[];

  constructor(private taskService: TaskService,
              private router: Router) {
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.taskService.getMyTasks().subscribe(res => {
      this.tasks = res;
    });
  }

  goToTask(task: Task) {
    localStorage.setItem("processInstanceId", task.processInstanceId);
    console.log(task);
    let url = "";
    console.log(task.name);

    let params = {};

    if (task.name === "Pay fee") url = '/pay_fee';

    if (task.name === "Choose journal") url = '/start_submission';
    if (task.name === "Enter paper info") url = '/submit_paper';
    if (task.name === "Correct paper") {
      url = '/submit_paper';
      params["reformat"] = true;
    }
    if (task.name.endsWith("revision")) url = '/submit_revision';

    if (task.name === "View paper info" || task.name === "Read paper pdf") url = '/paper_info';
    if (task.name === "Select reviewers") url = '/select_reviewers';
    if (task.name === "Analyze reviews" || task.name === "Analyze paper") url = '/analyze_reviews';
    if (task.name === "Review paper") url = '/review_paper';

    if (task.name === "Select Additional Reviewer") {
      url = '/select_reviewers';
      params["unassigned"]=true;
    }

    this.router.navigate([url, task.processInstanceId], {queryParams: params});
  }
}
