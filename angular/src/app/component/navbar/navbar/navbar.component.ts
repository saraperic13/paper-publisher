import {Component, OnInit} from '@angular/core';
import {NavbarLink} from "../model/NavbarLink";
import {AccountService} from "../../../service/account/account.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  username: string;

  constructor(private accountService: AccountService) {
  }

  links: NavbarLink[] = [];

  ngOnInit() {
    this.links = [];
    this.links.push(new NavbarLink('Journals', '/journals'));
  }

  userIsAuthor() {
    return this.accountService.hasRole('AUTHOR');
  }
  userIsEditor() {
    return this.accountService.hasRole('EDITOR');
  }

  userIsReviewer() {
    return this.accountService.hasRole('REVIEWER');
  }

  userIsChiefEditor() {
    return this.accountService.hasRole('CHIEF_EDITOR');
  }

  getUsername() {
    return this.username = this.accountService.getCurrentAccount().username;
  }

  userLoggedIn() {
    return this.accountService.isLoggedIn();
  }

  logout() {
    this.accountService.logout();
  }

}
