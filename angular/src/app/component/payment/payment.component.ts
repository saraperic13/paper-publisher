import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AppError} from "../../errors/app-error";
import {BadRequestError} from "../../errors/bad-request-error";
import {ForbiddenError} from "../../errors/forbidden-error";
import {NotFoundError} from "../../errors/not-found-error";
import {ToasterService} from "angular5-toaster/dist";
import {JournalService} from "../../service/journal/journal.service";
import {MembershipService} from "../../service/membership/membership.service";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  processInstanceId: string;
  customerId: string;

  paymenthubUrl: string = "localhost:8080/";
  ppUrl: string = "localhost:4200/";

  url = '\/\/' + this.ppUrl;
  urls: string;

  constructor(private activatedRoute: ActivatedRoute,
              private toasterService: ToasterService,
              private journalService: JournalService,
              private membershipService: MembershipService,
              private router: Router) {
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      let processInstanceId = params["processInstanceId"];
      localStorage.setItem("processInstanceId", processInstanceId);
      this.processInstanceId = processInstanceId;

      this.urls = '&successUrl=' + this.url + '\/return_url'
        + '&cancelUrl=' + this.url + '\/return_url\/' + this.processInstanceId;

      this.journalService.getPaymentHubCustomerId(processInstanceId).subscribe((res) => {
        this.customerId = res.id;
        console.log("CUSTOMER ID " + res.id);
      });
    });
  }

  createPlan() {
    let parameters = '?customerId=' + this.customerId + this.urls;
    window.location.href = '//' + this.paymenthubUrl + 'create_plan' + parameters;
  }

  payment() {
    console.log("pay");
    let parameters = '?customerId=' + this.customerId + '&price=' + "20" + '&currency=' + "USD" + this.urls;
    window.location.href = '//' + this.paymenthubUrl + 'home' + parameters;
  }

  subscribe() {
    console.log("subsc");
    let parameters = '?customerId=' + this.customerId + '&returnUrl=' + this.url + '\/return_url';
    window.location.href = '//' + this.paymenthubUrl + 'subscribe' + parameters;
  }

  cancel() {
    let dto = [];
    dto.push({"fieldId": "proceedPayment", "fieldValue": "false"});
    console.log(dto);
    this.submitForm(dto);
  }

  submitForm(dto) {
    this.membershipService.submitForm(this.processInstanceId, dto).subscribe(res => {
      console.log(res);
      this.router.navigate(["/my_tasks"]);
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  handleError(error: AppError) {
    if (error instanceof NotFoundError)
      this.toasterService.pop('error', 'Error', 'Not found!');
    else if (error instanceof ForbiddenError)
      this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
    else if (error instanceof BadRequestError)
      this.toasterService.pop('error', 'Error', 'Bad request!');
    else {
      this.toasterService.pop('error', 'Error', 'Error, look at console!');
      throw error;
    }
  }
}
