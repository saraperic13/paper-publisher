import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {ReadPaperComponent} from "./read-paper/read-paper.component";
import {PapersComponent} from "./papers/papers.component";
import {JournalsComponent} from "./journals/journals.component";
import {HomePageComponent} from "./home-page/home-page.component";

const homeRoutes: Routes = [
    {
      path: '',
      component: HomePageComponent,
      children: [
        {path: ':journalId/papers', component: PapersComponent},
        {path: 'paper/:paperId', component: ReadPaperComponent},
        {path: '', component: JournalsComponent}]
    }
  ];

@NgModule({
  imports: [
    RouterModule.forChild(homeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule {
}


