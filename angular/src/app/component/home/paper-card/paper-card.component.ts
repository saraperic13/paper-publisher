import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as FileSaver from 'file-saver';
import {Paper} from "../../../domain/Paper";
import {PaperService} from "../../../service/paper/paper.service";

@Component({
  selector: 'app-paper-card',
  templateUrl: './paper-card.component.html',
  styleUrls: ['./paper-card.component.css']
})
export class PaperCardComponent implements OnInit {

  @Input() paper: Paper;
  @Output() onReadPaperClicked = new EventEmitter();

  constructor(private paperService:PaperService) {
  }

  ngOnInit() {
  }

  download(paperId: number) {
    this.paperService.downloadPdf(paperId).subscribe((blob) => {
      FileSaver.saveAs(blob, "scientific_paper.pdf");
    });
  }

}
