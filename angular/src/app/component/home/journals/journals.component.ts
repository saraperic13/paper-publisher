import {Component, OnInit} from '@angular/core';
import {Journal} from "../../../domain/Journal";
import {JournalService} from "../../../service/journal/journal.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-journals',
  templateUrl: './journals.component.html',
  styleUrls: ['./journals.component.css']
})
export class JournalsComponent implements OnInit {

  journals: Journal[];

  constructor(private journalService: JournalService, private router: Router) {
  }

  ngOnInit() {
    this.journalService.getAll().subscribe((res) => {
      this.journals = res;
    });
  }

  details(journalId: number) {
    this.router.navigateByUrl(`journals/${journalId}/papers`);
  }

}
