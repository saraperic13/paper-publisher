import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadPaperComponent } from './read-paper.component';

describe('ReadPaperComponent', () => {
  let component: ReadPaperComponent;
  let fixture: ComponentFixture<ReadPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
