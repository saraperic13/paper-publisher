import {Component, OnInit} from '@angular/core';
import {Paper} from "../../../domain/Paper";
import {PaperService} from "../../../service/paper/paper.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-papers',
  templateUrl: './papers.component.html',
  styleUrls: ['./papers.component.css']
})
export class PapersComponent implements OnInit {

  papers: Paper[];
  journalId: number;

  constructor(private paperService: PaperService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.journalId = params['journalId'];
      this.getAll();
    });
  }

  getAll() {
    this.paperService.getAllPapersFromJournal(this.journalId).subscribe(res => {
      this.papers = res;
    });
  }

  readPaper(paperId: number) {
    this.router.navigate(['/paper', paperId]);
  }

}
