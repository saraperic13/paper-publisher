import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaperCardComponent} from './paper-card/paper-card.component';
import {PapersComponent} from './papers/papers.component';
import {JournalsComponent} from './journals/journals.component';
import {ReadPaperComponent} from './read-paper/read-paper.component';
import {HomeRoutingModule} from "./home-routing.module";
import {HomePageComponent} from './home-page/home-page.component';
import {JournalService} from "../../service/journal/journal.service";
import {PaperService} from "../../service/paper/paper.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptorService} from "../../interceptor/TokenInterceptorService";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ToasterModule} from "angular5-toaster/dist";

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToasterModule
  ],
  declarations: [
    PaperCardComponent,
    PapersComponent,
    JournalsComponent,
    ReadPaperComponent,
    HomePageComponent
  ],
  providers: [
    PaperService,
    JournalService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }],
})
export class HomeModule {
}
