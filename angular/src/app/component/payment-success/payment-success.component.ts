import { Component, OnInit } from '@angular/core';
import {MembershipService} from "../../service/membership/membership.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent implements OnInit {


  constructor(private membershipService:MembershipService,
              private router:Router) { }

  ngOnInit() {
    this.submitForm();
  }

  submitForm() {
    let dto = [{"fieldId": "proceedPayment", "fieldValue": "true"}];
    this.membershipService.submitForm(localStorage.getItem("processInstanceId"), dto).subscribe(res => {
      console.log(res);
      this.router.navigate(["/my_tasks"]);
    }, (error) => {
      console.log(error);
    });
  }


}
